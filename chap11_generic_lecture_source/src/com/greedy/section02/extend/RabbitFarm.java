package com.greedy.section02.extend;

//public class RabbitFarm<T> {
/* 인터페이스 상속 시 implements 키워드 대신 extends를 사용한다. */
//public class RabbitFarm<T implements Animal> {        // implements 사용 불가
//public class RabbitFarm<T extends Animal> {

/* 클래스 상속 시 extends 사용 */
public class RabbitFarm<T extends Rabbit> {

/* Reptile 자체를 타입 변수로 본다 */
//public class RabbitFarm<T extends Rabbit, Reptile> {

    private T animal;

    public RabbitFarm() {}

    public RabbitFarm(T animal) {
        this.animal = animal;
    }

    public T getAnimal() {
        return animal;
    }

    public void setAnimal(T animal) {
        this.animal = animal;
    }
}
