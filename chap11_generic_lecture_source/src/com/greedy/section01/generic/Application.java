package com.greedy.section01.generic;

public class Application {

    public static void main(String[] args) {

        /* 제네릭
         * 제네릭의 사전적인 의미는 일반적이라는 의미를 가지며
         * 자바에서 제네릭이란 데이터 타입을 일반화한다는 의미
         *
         * 제네릭은 클래스나 메소드에서 사용할 내부 데이터 타입을 컴파일 시에 지정하는 방법을 말한다.
         * 컴파일 시에 미리 타입 검사를 시행하게 되면 클래스나 메소드 내부에서 사용되는 객체의 타입 안정성을 높일 수 있으며
         * (잘못된 타입인 경우 컴파일 에러를 발생)
         * 반환값에 대한 타입변환 및 타입검사에 들어가는 코드 생략이 가능하다.
         * (instanceof 비교 및 다운캐스팅 작성 불필요)
         *
         *
         * */

        /* JDK 7부터 타입선언 시 타입변수가 작성되면 타입추론이 가능하기 때문에
         * 생성자 쪽의 타입을 생략하고 사용할 수 있다.
         * 단, 타입이 명시되지 않은 빈 다이아몬드 연산자는 사용해야한다.
         * */
        GenericTest<Integer> gt1 = new GenericTest<>();

        gt1.setValue(10);

        System.out.println(gt1.getValue());
        System.out.println(gt1.getValue() instanceof Integer);

        GenericTest<String> gt2 = new GenericTest<>();

        gt2.setValue("홍길동");

        System.out.println(gt2.getValue());
        System.out.println(gt2.getValue() instanceof String);

    }
}
