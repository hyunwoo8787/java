package com.greedy.section02.stream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Application3 {

    public static void main(String[] args) {

        /* FileOutputStream */

        /* 프로그램의 데이터를 파일로 내보내기 위한 용도의 스트림
         * 1바이트 단위로 데이터를 처리한다. */

        FileOutputStream fos = null;

        try {
            /* FileNotFoundException을 핸들링 해줘야 하지만
             * 실행해도 예외는 발생하지 않는다.
             * OutputStream의 경우 대상 파일이 존재하지 않으면 파일을 자동으로 생성해준다. */

            /* 두번째 인자로 true를 전달하면 이어쓰기가 된다.
             * false는 이어쓰기가 아닌 덮어쓰기이며, 기본값은 false이다. */
            fos = new FileOutputStream("src/com/greedy/section02/stream/testOutputStream.txt");

            /* byte배열을 이용해서 한번에 기록
             * 10 : 개행문자 (엔터) */
            byte[] test = {
                    -19,
                    -107,
                    -100,
                    -22,
                    -72,
                    -128,
                    10,
            };
            fos.write(test);
            fos.write(test, 0, 3);
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
