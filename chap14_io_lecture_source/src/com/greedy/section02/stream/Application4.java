package com.greedy.section02.stream;

import java.io.FileWriter;
import java.io.IOException;

public class Application4 {

    public static void main(String[] args) {

        /* FileWriter
         *
         * 프로그램의 데이터를 파일로 내보내기 위한 용도의 스트림이다.
         * 한 글자 단위로 데이터를 처리한다. */

        /* 두번째 인자로 true를 전달하면 이어쓰기가 된다. */
        try (FileWriter fw = new FileWriter("src/com/greedy/section02/stream/testWriter.txt", true)) {
            fw.append("abcd\n");

            } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
