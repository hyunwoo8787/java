package com.greedy.section03.filterstream;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class MyOutputStream extends ObjectOutputStream {

    private static boolean isTrue;

    public MyOutputStream(String name) throws IOException {
        super(new BufferedOutputStream(
                new FileOutputStream(name, isTrue)));
    }

    public MyOutputStream(String name, boolean append) throws IOException {
        super(new BufferedOutputStream(
              new FileOutputStream(name, (isTrue = new File(name).exists() ? append : false))));
    }

    @Override
    protected void writeStreamHeader() throws IOException {
        if (!isTrue) {
            super.writeStreamHeader();
        }
    }
}
