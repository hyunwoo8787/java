package com.greedy.section03.filterstream;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.greedy.section03.filterstream.vo.MemberVO;

public class Application4 {

    public static void main(String[] args) {

        /* 객체 단위로 입출력을 하기 위한 ObjectInputStream / ObjectOutputStream */

        MemberVO[] outputMembers = {
                new MemberVO("user01", "pass01", "홍길동", "hong777@greedy.com", 25, '남', 1250.7),
                new MemberVO("user02", "pass02", "유관순", "korea777@greedy.com", 16, '여', 1221.7),
                new MemberVO("user03", "pass03", "이순신", "leesonsin777@greedy.com", 22, '남', 1234.7),
        };

        ObjectOutputStream objOut = null;

        try {
//            objOut = new ObjectOutputStream(
//                        new BufferedOutputStream(
//                            new FileOutputStream("src/com/greedy/section03/filterstream/testObjectStream.txt", true)));
            objOut = new MyOutputStream("src/com/greedy/section03/filterstream/testObjectStream.txt", true);

            for (MemberVO memberVO : outputMembers) {
                objOut.writeObject(memberVO);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (objOut != null) {
                try {
                    objOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        /* VO에 implements java.io.Serializable을 추가해주지 않으면 NotSerializableException이 발생
         * --> 직렬화 처리를 해주지 않아서 발생하는 에러이다.
         *
         * 직렬화란?
         * 자바 시스템 내부에서 사용되는 객체 또는 데이터를 외부에서도 사용할 수 있도록
         * 바이트(byte) 형태로 데이터를 변환하는 기술을 말한다.
         * 반대로 바이트로 변환된 데이터를 다시 객체로 변환하는 기술을 역직렬화라고 한다. */

        try (ObjectInputStream objIn = new ObjectInputStream(
                                            new BufferedInputStream(
                                                    new FileInputStream("src/com/greedy/section03/filterstream/testObjectStream.txt")))) {

            MemberVO[] inputMembers = new MemberVO[6];

            for (int i = 0; i < inputMembers.length; i++) {
                inputMembers[i] = (MemberVO) objIn.readObject();
                System.out.println(inputMembers[i]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (EOFException e) {
            System.out.println("파일 읽기 완료.");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
