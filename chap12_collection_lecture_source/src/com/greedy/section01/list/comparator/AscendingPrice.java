package com.greedy.section01.list.comparator;

import java.util.Comparator;

import com.greedy.section01.list.dto.BookDTO;

public class AscendingPrice implements Comparator<BookDTO> {

    @Override
    public int compare(BookDTO o1, BookDTO o2) {

        /* sort()에서 내부적으로 사용하는 메소드 */

        /* 비교 대상 두 인스턴스의 가격을 오름차순 정렬 되기 위해서는
         * 앞의 가격이 더 적은 가격이여야 한다.
         *
         * 만약 뒤의 가격이 더 적은 경우에는 두 인스턴스의 순서를 바꿔야한다.
         * 그 때 두 값을 바꾸라는 신호로 양수를 보내주게 되면 정렬 시 순서를 바꾸는 조건으로 사용된다. */

        /* 양수, 음수 형태로 두 비교값이 순서를 바꿔야 하는지를 알려주기 위한 용도의 변수 */
        int result = 0;

        if (o1.getPrice() > o2.getPrice()) {
            /* 오름차순을 위해 순서를 바꿔야하는 경우 양수를 반환 */
            result = 1;
        } else if (o1.getPrice() < o2.getPrice()) {
            /* 이미 오름차순일 경우 음수를 반환 */
            result = -1;
        }

        /* 순서를 바꿔야 하는 경우에만 양수를 반환 */
        return result;
    }

}
