package com.greedy.section01.list.run;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.greedy.section01.list.comparator.AscendingPrice;
import com.greedy.section01.list.dto.BookDTO;

public class Application2 {

    public static void main(String[] args) {
        /* ArrayList의 용법과 정렬 확인 */
//        BookDTO[] bookArr = new BookDTO[3];

        /* 여러 권의 책 목록을 관리할 ArrayList인스턴스 생성 */
        List<BookDTO> bookList = new ArrayList<>();

        /* 도서 정보 추가 */
        bookList.add(new BookDTO(1, "홍길동전", "허균", 50_000));
        bookList.add(new BookDTO(2, "목민심서", "정약용", 30_000));
        bookList.add(new BookDTO(3, "동의보감", "허준", 40_000));
        bookList.add(new BookDTO(4, "삼국사기", "김부식", 45_000));
        bookList.add(new BookDTO(5, "삼국유사", "일연", 58_000));

        for(BookDTO book : bookList) {
            System.out.println(book);
        }

        /* Comparator 인터페이스를 상속받아 정렬의 기준을 정해준 뒤 List의 default메소드인 sort()메소드의
         * 인자로 정렬기준이 되는 인스턴스를 넣어주게 되면 내부적으로 우리가 오버라이딩한 메소드가 동작하게되어
         * 그것을 기준으로 정렬이 된다.*/
        bookList.sort(new AscendingPrice());

        System.out.println("========================================================");
        for(BookDTO book : bookList) {
            System.out.println(book);
        }

        /* 인터페이스를 구현한 클래스를 재사용하는 경우 AscendingPrice 클래스처럼 작성하면 되지만
         * 한번만 사용하기 위해서는 익명클래스(Anonymous)를 이용할 수도 있다. * */

        /* 가격순 내림차순 정렬 */
        bookList.sort(new Comparator<BookDTO>() {
            @Override
            public int compare(BookDTO o1, BookDTO o2) {
                return o2.getPrice() - o1.getPrice();
            }
        });

        System.out.println("========================================================");
        for(BookDTO book : bookList) {
            System.out.println(book);
        }

        /* 제목순 오름차순 정렬 */
        bookList.sort(new Comparator<BookDTO>() {

            @Override
            public int compare(BookDTO o1, BookDTO o2) {
                /* 문자열은 대소비교를 할 수 없다.
                 * 문자 배열로 변경 후 인덱스 하나하나를 비교해서 어느 것이 더 큰 값인지 확인해야하는데
                 * String클래스의 compareTo() 메소드에서 이미 정의를 해놓았다. */

                /* 앞에 값이 더 작은 경우 (즉, 바꾸지 않아도 되는 경우) 음수 반환,
                 * 같으면 0반환
                 * 앞에 값이 더 큰 경우 양수 반환 (즉, 바꿔야하는 경우) */
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });

        System.out.println("========================================================");
        for(BookDTO book : bookList) {
            System.out.println(book);
        }

        /* 제목순 내림차순 정렬 */
        bookList.sort(new Comparator<BookDTO>() {

            @Override
            public int compare(BookDTO o1, BookDTO o2) {
                return o2.getTitle().compareTo(o1.getTitle());
            }
        });

        System.out.println("========================================================");
        for(BookDTO book : bookList) {
            System.out.println(book);
        }

        // Ascending(ASC) :      오름차순
        // Descending(DESC) :    내림차순
    }
}
