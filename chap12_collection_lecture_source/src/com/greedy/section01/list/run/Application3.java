package com.greedy.section01.list.run;

import java.util.LinkedList;
import java.util.List;

public class Application3 {

    public static void main(String[] args) {
        /* LinkedList
         * ArrayList가 배열을 이용해서 발생할 수 있는 성능적인 단점을 보완하고자 고안되었다.
         * LinkedList 내부는 이중 연결리스트로 구현되어 있다.
         *
         * 단일 연결 리스트
         * : 저장한 요소가 순서를 유지하지 않고 저장되지만 이러한 요소들 사이를 링크로 연결하여
         *   구성하며 마치 연결된 리스트 형태인 것처럼 만든 자료구조이다.
         *   요소의 저장과 삭제 시 다음 요소를 가리키는 참조링크만 변경하면 되기 때문에
         *   요소의 저장과 삭제가 빈번히 일어나느 경우 ArrayList보다 성능면에서 우수하다.
         *
         *   하지만 단일 연결리스트는 다음 요소만 링크하기 떄문에 이전 요소로 접근하기가 어렵다
         *   이를 보완하고자 만든 것이 이중 연결 리스트이다.
         *
         * 이중 연결 리스트
         * : 단일 연결 리스트는 다음 요소만 링크하는 반면 이중 연결 리스트는 이전 요소도 링크하여
         *   이전 요소로 접근하기 쉽게 고안된 자료구조이다.
         *
         *   LinkedList는 이중 연결 리스트를 구현한 것이며 역시 List인터페이스를 상속받아서
         *   ArrayList와 사용하는 방법이 거의 유사하다. */

        List<String> linkedList = new LinkedList<>();

        /* 요소를 추가할 때 add를 이용 */
        linkedList.add("Apple");
        linkedList.add("Banana");
        linkedList.add("Orange");
        linkedList.add("Mango");
        linkedList.add("Grape");

        System.out.println("linkedList : " + linkedList);

        /* 저장된 요소의 개수는 size() */
        System.out.println("linkedList의 size : " + linkedList.size());

        /* for문과 size()를 이용해서 반복문을 활용할 수 있다.
         * 요소를 꺼내올 때는 get()을 사용하며, 인자로 전달하는 정수는 인덱스처럼 사용하면 된다. */
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.println(i + " : " + linkedList.get(i));
        }

        /* 요소를 제거할 때는 remove() 메소드를 이용하여 인덱스를 활용한다. */
        linkedList.remove(1);

        /* 향상된 for문 사용도 가능하다. */
        for (String string : linkedList) {
            System.out.println(string);
        }

        /* set()메소드를 이용해서 요소를 수정 */
        linkedList.set(0, "fineapple");

        System.out.println(linkedList);

        /* isEmpty() 메소드를 이용해서 list가 비어있는지 확인 */
        System.out.println(linkedList.isEmpty());

        /* 리스트 내 요소를 모두 제거하는 메소드 */
        linkedList.clear();

        System.out.println(linkedList);

        /* isEmpty() 메소드를 이용해서 list가 비어있는지 확인 */
        System.out.println(linkedList.isEmpty());
    }
}
