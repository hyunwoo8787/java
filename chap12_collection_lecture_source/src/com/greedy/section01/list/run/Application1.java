package com.greedy.section01.list.run;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Application1 {

    public static void main(String[] args) {

        /* 컬렉션 프레임워크
         * 자바에서 컬렉션 프레임워크는 여러 개의 다양한 데이터들을 쉽고 효과적으로 처리할 수 있도록
         * 표준화된 방법을 제공하는 클래스들의 집합을 의미한다.
         *
         * Collection Framework는 크게 3가지 인터페이스 중 한가지를 상속받아 구현해 놓았다.
         * 1. List 인터페이스
         * 2. Set 인터페이스
         * 3. Map 인터페이스
         *
         * List 인터페이스와 Set 인터페이스는 Collection 인터페이스를 상속받아 정의하고 있다.
         * 하지만 Map은 구조상의 차이로 Collection 인터페이스에서 정의하고 있지 않다.
         * */

        /* ArrayList
         * 가장 많이 사용되는 컬렉션 클래스이다.
         * JDK 1.2부터 제공된다.
         * 내부적으로 배열을 이용하여 요소를 관리하며, 인덱스를 이용해 배열 요소에 빠르게 접근할 수 있다.
         *
         * ArrayList는 배열의 단점을 보완하기 위해 만들어졌다.
         * 배열은 크기를 변경할 수 없고, 요소의 추가, 삭제, 정렬 들이 복잡하다는 단점을 가지고 있다.
         *
         * ArrayList는 이러한 배열의 단점을 보완하고자 크기변경(새로운 더 큰 배열을 만들고 옮기기), 요소의 추가,
         * 삭제, 정렬 기능을 미리 메소드로 구현해서 제공하고 있다.
         * --> 자동으로 수행되는 것이지 속도가 빨라지는 것은 아니다.
         * */

        ArrayList alist = new ArrayList();
        /* 다형성을 적용하여 상위 레퍼런스로 ArrayList객체를 만들 수도 있다.
         *
         * List 인터페이스 하위의 다양한 구현제들로 타입변경이 가능하기 때문에
         * 레퍼런스 타입은 List로 해두는 것이 더 유연한 코드를 작성하는 것이다.
         * */
        List list = new ArrayList();
        Collection clist = new ArrayList();

        /* ArrayList는 저장 순서가 유지되며 index(순번)이 적용된다.
         * ArrayList는 Object클래스의 하위 타입 인스턴스를 모두 저장할 수 있다. */
        alist.add("apple");  // alist[0] = "apple";
        alist.add(123);      // 오토박싱 처리됨 (값 -> 객체)
        alist.add(45.67);
        alist.add(new Date());
        alist.add(alist);
        alist.remove(new Date());
        alist.remove(Integer.valueOf(123));
        alist.remove(alist);
        System.out.println("alist : " + alist.toString());

        /* ArrayList의 크기는 size()메소드로 확인
         * 단 size()메소드는 배열의 크기가 아닌 요소의 갯수를 반환한다. */

        System.out.println("alist의 size : " + alist.size());

        for (Iterator iterator = alist.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next());
        }

        /* ArrayList는 데이터의 중복 저장을 허용한다.
         * 배열과 같이 인덱스로 요소들을 고나리하기 때문에 인덱스가 다른 위치에 동일한 값을 저장하는 것이 가능하다. */
        alist.add("apple");
        System.out.println("alist : " + alist);

        /* 원하는 인덱스 위치에 값을 추가할 수도 있다.
         * 값을 중간에 추가하는 경우 인덱스 위치에 덮어쓰는 것이 아니고
         * 새로운 값이 들어가는 인덱스 위치에 값을 넣고 이후 인덱스는 하나씩 뒤로 밀리게 된다. */
        alist.add(1, "banana");
        System.out.println("alist : " + alist);

        /* 저장된 값을 삭제할 때는 remove() 메소드를 사용한다.
         * 중간 인덱스의 값을 삭제하는 경우 자동으로 인덱스를 하나씩 앞으로 당긴다. */
        alist.remove(2);
        System.out.println("alist : " + alist);

        /* 지정된 위치의 값을 수정할 때에도 인덱스를 활용할 수 있다. */
        alist.set(1, Boolean.valueOf(true));
        System.out.println("alist : " + alist);

        List<String> stringList = new ArrayList<>();
        stringList.add("Apple");
//        stringList.add(123);
        stringList.add(String.valueOf(123));
        stringList.add("banana");
        stringList.add("orange");
        stringList.add("mango");
        stringList.add("grape");

        System.out.println("stringList : " + stringList);

        /* Collection 인터페이스가 아닌 Collections 클래스이다.
         * Collection에서 사용되는 기능들을 static메소드들로 구현한 클래스이다.
         * 인터페이스명 뒤에 s가 붙은 클래스들은 관례상 비슷한 방식으로 작성된 클래스를 의미하게 된다. */
        Collections.sort(stringList);
        System.out.println("stringListSort : " + stringList); // 오름차순

        Collections.sort(stringList, Collections.reverseOrder(String.CASE_INSENSITIVE_ORDER));
        System.out.println("stringListSort : " + stringList); // 내림차순

        stringList = new LinkedList<>(stringList);

        /* Iterator 반복자 인터페이스를 활용해서 역순으로 정렬한다.
         *
         * Iterator란
         * Collection 인터페이스의 iterator() 메소드를 이용해서 인스턴스를 생성할 수 있다.
         * 컬렉션에서 값을 읽어오는 방식을 통일된 방식으로 제공하기 위해서 사용한다.
         * 반복자라고 불리우며, 반복문을 이용해서 목록을 하나씩 꺼내는 방식으로 사용하기 위함이다.
         *
         * 인덱스로 관리되는 컬렉션이 아닌 경우에는 반복문을 사용해서 요소에 하나씩 접근할 수 없기 때문에
         * 인덱스를 사용하지 않고도 반복문을 사용하기 위한 목록을 만들어주는 역할이라고 보면 된다.
         *
         * hasNext() : 다음 요소를 가지고 있는 경우 true, 더 이상 요소가 없는 경우 false 반환
         * next() : 다음 요소를 반환
         * */

        Iterator<String> dIter = ((LinkedList<String>) stringList).descendingIterator();

//        while (dIter.hasNext()) {
//            System.out.println(dIter.next());
//        }
//
//        /* 위에서 한번 꺼내면 다시 쓸 수 없다. */
//        while (dIter.hasNext()) {
//            System.out.println(dIter.next());
//        }

        List<String> descList = new ArrayList<>();

        while (dIter.hasNext()) {
            descList.add(dIter.next());
        }

        System.out.println("descList : " + descList);
    }
}
