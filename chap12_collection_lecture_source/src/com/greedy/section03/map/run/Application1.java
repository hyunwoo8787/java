package com.greedy.section03.map.run;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Application1 {

    public static void main(String[] args) {

        /* Map 인터페이스의 특징
         *
         * Collection 인터페이스와는 다른 저장 방식을 가진다.
         * 키(key)와 값(value)를 하나의 쌍으로 저장하는 방식을 사용한다.
         *
         * 키(key)란?
         * 값(value)를 찾기 위한 이름 역할을 하는 객체를 의미한다.
         *
         * 1. 요소의 저장 순서를 유지하지 않는다.
         * 2. 키는 중복을 허용하지 않지만, 키가 다르면 중복되는 값은 저장 가능하다.
         *
         * HashMap, HashTable 등의 대표적인 클래스가 있다.
         * HashMap이 가장 많이 사용되며 HashTable은 JDK1.0부터 제공되며
         * HashMap과 동일하게 동작한다. */

        HashMap hmap = new HashMap();
        Map hmap2 = new HashMap();

        /* 키와 값을 쌍으로 저장
         * 키와 값 둘다 반드시 객체여야 한다.
         * */
        hmap.put("one", new Date());
        hmap.put(12, "red apple");
        hmap.put(33,123);   /* 숫자들은 12 => new Integer(12)로 오토박싱된 것이다. */

        System.out.println("hmap : " + hmap);

        /* 키는 중복 저장 되지 않는다. (set의 특징) : 최근 키로 override된다. (덮어쓴다.) */
        hmap.put(12, "yellow banana");
        System.out.println("hmap : " + hmap);

        hmap.put(11, "yellow banana");
        hmap.put(9, "yellow banana");
        System.out.println("hmap : " + hmap);

        System.out.println("키 11에 대한 객체 : " + hmap.get(11));

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "first");
        map.put(2, "second");
        System.out.println("map : " + map);
        System.out.println("map의 키값이 1인거 : " + map.get(1));

        /* 키 값을 가지고 삭제를 처리 */
        hmap.remove(9);
        System.out.println("hmap : " + hmap);

        System.out.println("hmap에 저장된 객체 수 : " + hmap.size());

        HashMap<String, String> hmap3 = new HashMap<>();
        hmap3.put("one", "java 11");
        hmap3.put("two", "oracle 18c");
        hmap3.put("three", "jdbc");
        hmap3.put("four", "html5");
        hmap3.put("five", "css3");

        /* 1. keySet()을 이용해서 키만 따로 set으로 만들고, iterator()로 키에 대한 목록을 만든다. */
//        Set<String> keys = hmap3.keySet();
        Iterator<String> keyIter = hmap3.keySet().iterator();

        while (keyIter.hasNext()) {

            String key = keyIter.next();
            String value = hmap3.get(key);

            System.out.println(key + " = " + value);
        }

        /* 2. 저장된 value 객체들만 values() 메서드를 사용해 Collection으로 만든다. */
        Collection<String> values = hmap3.values();
        /* Iterator()로 목록 만들어서 처리 */
        keyIter = values.iterator();

        while (keyIter.hasNext()) {
            System.out.println(keyIter.next());
        }

        /* toArray()메소드를 이용해서 배열로 만들어서 처리 */
        Object[] valueArr = values.toArray();

        for (int i = 0; i < valueArr.length; i++) {
            System.out.println(i + " : " + valueArr[i]);
        }

        /* Map의 내부클래스인 EntrySet을 이용 : entrySet() */
        Set<Map.Entry<String, String>> set = hmap3.entrySet();
        // Entry : 키 객체와 값 객체를 쌍으로 묶은 것
        Iterator<Map.Entry<String, String>> entryIter = set.iterator();

        while (entryIter.hasNext()) {

            Map.Entry<String, String> entry = entryIter.next();
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}
