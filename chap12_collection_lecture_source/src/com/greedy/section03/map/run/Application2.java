package com.greedy.section03.map.run;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Application2 {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        Properties props = new Properties();

        props.setProperty("driver", "oracle.jdbc.driver.OracleDriver");
        props.setProperty("url", "jdbc:oracle:thin:@127.0.0.1:1521:xe");
        props.setProperty("user", "student");
        props.setProperty("password", "student");

        System.out.println("props : " + props);

        /* store() : Properties 객체 안에 있는 값을 이용해서 파일을 생성할 수 있다. */

        /* Writer 객체는 자바에서 파일을 생성할 때 사용하는 클래스 중에 하나이다. */
        props.store(new FileOutputStream("driver.dat"), "jdbc driver");
        props.store(new FileWriter("driver.txt"), "jdbc driver");
        props.storeToXML(new FileOutputStream("driver.xml"), "jdbc driver");

        /* 파일로부터 읽어와서 Properties에 기록 */
        Properties prop2 = new Properties();
        prop2.load(new FileInputStream("driver.dat"));
        System.out.println("prop2 : " + prop2);

        Properties prop3 = new Properties();
        prop3.load(new FileReader("driver.txt"));
        System.out.println("prop3 : " + prop3);

        Properties prop4 = new Properties();
        prop4.loadFromXML(new FileInputStream("driver.xml"));
        System.out.println("prop4 : " + prop4);

        /* Properties의 모든 키 값 목록을 대상 스트림에 내보내기한다. */
        prop2.list(System.out);
    }
}
