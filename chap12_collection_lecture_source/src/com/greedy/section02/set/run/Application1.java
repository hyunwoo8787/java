package com.greedy.section02.set.run;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Application1 {

    public static void main(String[] args) {
        /* Set 인터페이스를 구현한 Set컬렉션 클래스의 특징
         * 1. 요소의 저장 순서를 유지하지 않는다.
         * 2. 같은 요소의 중복 저장을 허용하지 않는다. (null값도 중복하지 않게 하나의 null만 저장한다.)
         * */

        /* HashSet 클래스
         * Set컬렉션 클래스에서 가장 많이 사용되는 클래스 중 하나이다.
         * JDK 1.2부터 제공되고 있으면 해시 알고리즘을 사용하여 검색 속도가 빠르다는 장점을 가진다. */
        HashSet<String> hset = new HashSet<>();

        /* 다형성 적용하여 상위 인터페이스를 타입으로 사용 가능하다. */
        Set<String> hset2 = new HashSet<>();
        Collection<String> hset3 = new HashSet<>();

        hset.add(new String("java"));
        hset.add(new String("oracle"));
        hset.add(new String("jdbc"));
        hset.add(new String("html"));
        hset.add(new String("css"));

        /* toString() 메소드가 오버라이딩 되어 있다. */
        /* 저장 순서 유지가 안된다. */
        System.out.println("hset : " + hset);

        /* 중복 허용 안된다. */
        hset.add(new String("java"));

        System.out.println("hset : " + hset);
        System.out.println("저장된 객체수 : " + hset.size());
        System.out.println("포함확인 : " + hset.contains(new String("oracle")));

        /* 저장된 객체를 하나씩 꺼내는 기능은 제공하지 않는다.
         *
         * 반복문을 이용한 연속 처리 하는 방법
         * 1. toArray()배열로 바꾸고 for loop 사용 */

        Object[] arr = hset.toArray();

        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + " : " + arr[i]);
        }

        /* 2. iterator() 메소드로 목록 만들어 연속 처리 */
        Iterator<String> iter = hset.iterator();

        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        hset.clear();
        System.out.println("empty? : " + hset.isEmpty());
    }
}
