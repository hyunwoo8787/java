package com.greedy.section02.abstractclass;

public class Application {

    public static void main(String[] args) {
        /* 추상클래스와 추상메소드
         *
         * 추상메소드를 0개 이상 포함하는 클래스를 추상클래스라고 한다.
         *
         * 추상클래스는 클래스 선언부에 abstract 키워드를 명시해줘야한다.
         * 추상클래스는 인스턴스 생성 불가
         *
         * 추상클래스를 상속받아 구현할 때는 extends 키워드를 사용
         * 자바에서는 extends로 클래스를 상속 받을 시 하나의 부모 클래스만 가질 수 있다. (단일상속)
         *
         * 추상메소드란?
         * 메소드의 선언부만 있고 구현부가 없는 메소드
         *
         * 추상메소드의 경우 반드시 abstract 키워드를 메소드 헤드에 작성해야한다.
         * public abstract void method();
         * */

//        Product product = new Product(); // 추상클래스는 인스턴스 생성 불가
        SmartPhone smartPhone = new SmartPhone();

        System.out.println(smartPhone instanceof SmartPhone);
        System.out.println(smartPhone instanceof Product);

        Product product = new SmartPhone();
        product.abstractMethod(); // 동적바인딩
        product.nonStaticMethod();
        Product.staticMethod();

        /* 추상클래스를 왜 쓰는가
         * 추상클래스와 추상메소드는 오버라이딩에 대한 강제성이 부여된다.
         * 따라서 여러 클래스들을 그룹화하여 필수 기능을 정의하여 강제성을 부여해 개발 시 일관된 인터페이스를 재공할 수 있다.
         * */
    }
}
