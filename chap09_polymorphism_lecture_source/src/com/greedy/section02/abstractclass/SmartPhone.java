package com.greedy.section02.abstractclass;

public class SmartPhone extends Product {

    public SmartPhone() {}

    @Override
    public void abstractMethod() {
        System.out.println("Product클래스의 abstractMethod를 오버라이딩 한 메소드 호출...");
    }

    public void printSmartPhone() {
        System.out.println("SmartPhone클래스의 printSmartPhone 메소드 호출...");
    }

}
