package com.greedy.section01.polymorphism;

public class Application2 {

    public static void main(String[] args) {

        Animal[] animals = new Animal[5];

        animals[0] = new Rabbit();
        animals[1] = new Tiger();
        animals[2] = new Animal();
        animals[3] = new Rabbit();
        animals[4] = new Tiger();

        for (Animal animal : animals) {
            animal.cry();
        }

        for (Animal animal : animals) {
            if (animal instanceof Rabbit) {
                ((Rabbit) animal).jump();
            } else if (animal instanceof Tiger) {
                ((Tiger) animal).bite();
            }
        }
    }
}
