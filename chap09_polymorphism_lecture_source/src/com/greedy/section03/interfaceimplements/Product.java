package com.greedy.section03.interfaceimplements;

public class Product extends java.lang.Object implements InterProduct, java.io.Serializable {

    @Override
    public void nonStaticMethod() {
        System.out.println("InterProduct의 nonStaticMethod 오버라이딩한 메소드 호출...");
    }

    @Override
    public void nonStaticMethod2() {
        System.out.println("InterProduct의 nonStaticMethod2 오버라이딩한 메소드 호출...");
    }

    /* static 메소드는 오버라이딩 할 수 없다. */

    /* default 키워드는 인터페이스에서만 작성 가능하다. */
//    public default void defaultMethod() {}

    /* default 키워드를 제거하면 오버라이딩 가능하다. */
    @Override
    public void defaultMethod() {
        System.out.println("Product 클래스의 defaultMethod 메소드 호출...");
    }
}
