package com.greedy.section03.interfaceimplements;

public interface InterProduct
        extends java.io.Serializable/* , Cloneable, Comparator<T> */ {

    /* 인터페이스가 인터페이스를 상속받을 시에는 extends 키워드를 이용하여
     * 이 때는 여러 인터페이스를 다중 상속 받을 수 있다.
     * */

    /* 인터페이스는 상수 필드만 작성 가능하다.
     *
     * public static final 제어자 조합을 상수 필드라고 한다.*/

    public static final int MAX_NUM = 100;

    /* 상수 필드만 가질 수 있기 때문에 모든 필드는 묵시적으로 public static final 이다. */
    int MIN_NUM = 10;

    /* 인터페이스는 기본생성자를 가질 수 없다. */
//    public InterProduct() {}

    /* 인터페이스는 메소드 구현부가 있는 non-static 메소드를 가질 수 없다 */
//    public void nonStaticMethod() {}

    /* 추상메소드만 작성이 가능하다
     * 접근제어자는 public만 가능 */
    public abstract void nonStaticMethod();

    /* 추상메소드만 작성할 수 있기 때문에 묵시적으로 public abstract을 붙여준다. */
    void nonStaticMethod2();

    /* 추가된 문법 (JDK 1.8 이후 추가된 기능) */
    public static void staticMethod() {
        System.out.println("InterProduct클래스의 staticMethod 호출...");
    }

    public default void defaultMethod() {
        System.out.println("InterProduct클래스의 defaultMethod 호출...");
    }
}
