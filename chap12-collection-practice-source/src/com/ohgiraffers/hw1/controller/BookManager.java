package com.ohgiraffers.hw1.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.ohgiraffers.hw1.comparator.AscBookNo;
import com.ohgiraffers.hw1.comparator.AscBookTitle;
import com.ohgiraffers.hw1.comparator.DescBookNo;
import com.ohgiraffers.hw1.comparator.DescBookTitle;
import com.ohgiraffers.hw1.model.dto.BookDTO;

public class BookManager {

    private ArrayList<BookDTO> booklist;
    private Scanner sc = new Scanner(System.in);

    public BookManager() {
        booklist = new ArrayList<>();
    }

    public void addBook(BookDTO book) {
        booklist.add(book);
    }

    public void deleteBook(int index) {

        for (int i = 0; i < booklist.size(); i++) {
            if (booklist.get(i).getbNo() == index) {
                booklist.remove(i);
                System.out.println("삭제 되었습니다.");

                return;
            }
        }

        System.out.println("입력하신 도서 번호는 존재하지 않습니다.");
    }

    public void searchBook(String btitle) {

        for (Iterator<BookDTO> iterator = booklist.iterator(); iterator.hasNext();) {
            BookDTO bookDTO = iterator.next();

            if (bookDTO.getTitle().contains(btitle)) {
                System.out.println(bookDTO.toString());

                return;
            }
        }

        System.out.println("조회된 도서가 목록에 없습니다.");
    }

    public void displayAll() {
        int size = booklist.size();

        if (size == 0) {
            System.out.println("조회결과가 없습니다.");
            return;
        }

        for (Iterator<BookDTO> iterator = booklist.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next());
        }
    }

    public List<BookDTO> sortedBookList(int type) {
         switch (type) {
             case 1: Collections.sort(booklist, new AscBookNo()); break;
             case 2: Collections.sort(booklist, new DescBookNo()); break;
             case 3: Collections.sort(booklist, new AscBookTitle()); break;
             case 4: Collections.sort(booklist, new DescBookTitle()); break;
             default: System.out.println("잘못된 번호를 입력했습니다.");
         }

        // selectTypeSortedBookList(type);

        return booklist;
    }

    private void selectTypeSortedBookList(int type) {

        Comparator<BookDTO> comparator;

        switch (type) {
            case 1: comparator = new AscBookNo(); break;
            case 2: comparator = new DescBookNo(); break;
            case 3: comparator = new AscBookTitle(); break;
            case 4: comparator = new DescBookTitle(); break;
            default: System.out.println("잘못된 값을 입력하셨습니다."); return;
        }

        for (int i = 1; i < booklist.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (0 > comparator.compare(booklist.get(i), booklist.get(j))) {
                    BookDTO tmp = booklist.get(i);
                    booklist.set(i, booklist.get(j));
                    booklist.set(j, tmp);
                }
            }
        }
    }

    public void printBookList(List<BookDTO> printList) {
        System.out.println();
        for (Iterator<BookDTO> iterator = booklist.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next());
        }
    }
}
