package com.ohgiraffers.hw1.view;

import java.util.List;
import java.util.Scanner;

import com.ohgiraffers.hw1.controller.BookManager;
import com.ohgiraffers.hw1.model.dto.BookDTO;

public class BookMenu {

    private Scanner sc = new Scanner(System.in);
    private BookManager bm = new BookManager();

    public BookMenu() {}

    public void mainMenu() {
        loop:
        while (true) {
            System.out.println();
            System.out.println("*** 도서 관리 프로그램 ***");
            System.out.println("1. 새 도서 추가");                   // addBook (inputBook()이 리턴한 객체) 실행
            System.out.println("2. 도서정보 정렬 후 출력");            // printBookList() 실행 =>인자로 selectSortedBook()하여 리턴받은 리스트 객체사용
            System.out.println("3. 도서 삭제");                     // deleteBook (inputBookNo()이 리턴한 도서 번호) 실행
            System.out.println("4. 도서 검색출력");                   // searchBook (inputBookTitle()이 리턴한 도서 제목) 실행 => 결과가 있는경우 해당 도서를 출력하고, 결과가 없는 경우 “조회한 도서가 목록에 없습니다.”
            System.out.println("5. 전체 출력");                      // displayAll() 실행 => 비어있을 경우 “출력결과가 없습니다.” 비어있지 않은 경우 전체 출력
            System.out.println("6. 끝내기");                       // main()으로 리턴 메뉴 번호 선택 : >> 입력 받음
            System.out.print("메뉴 번호 선택 : ");
            int num = sc.nextInt();

            switch (num) {
                case 1: bm.addBook(inputBook()); break;
                case 2: bm.printBookList(selectSortedBook()); break;
                case 3: bm.deleteBook(inputBookNo()); break;
                case 4: bm.searchBook(inputBookTitle()); break;
                case 5: bm.displayAll(); break;
                case 6: System.out.println("프로그램을 종료합니다."); break loop;
                default: System.out.println("잘못된 번호를 입력하셨습니다. 다시 입력해주세요.");
            }
        }
    }

    public BookDTO inputBook() {
        System.out.print("도서 번호(ISBN) : ");
        int bNo = sc.nextInt();
        System.out.print("도서 제목 : ");
        String title = sc.next();
        System.out.print("도서 장르 (1:인문 / 2:자연과학 / 3:의료 / 4:기타) : ");
        int category = sc.nextInt();
        System.out.print("도서 저자 : ");
        String author = sc.next();

        return new BookDTO(bNo, category, title, author);

    }

    public int inputBookNo() {
        System.out.print("도서 번호 : ");
        int bNo = sc.nextInt();

        return bNo;
    }

    public String inputBookTitle() {
        System.out.print("도서 제목 : ");
        String name = sc.next();

        return name;
    }

    public List<BookDTO> selectSortedBook() {
        System.out.println("1. 도서번호(ISBN)으로 오름차순정렬");
        System.out.println("2. 도서번호(ISBN)으로 내림차순정렬");
        System.out.println("3. 책 제목으로 오름차순정렬");
        System.out.println("4. 책 제목으로 내림차순정렬");
        System.out.print("도서 정렬방식을 입력해주세요. : ");
        int num = sc.nextInt();
        return bm.sortedBookList(num);
    }
}
