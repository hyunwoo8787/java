package com.ohgiraffers.hw2.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

import com.ohgiraffers.hw2.comparator.AscCategory;
import com.ohgiraffers.hw2.comparator.DescCategory;
import com.ohgiraffers.hw2.model.dto.BookDTO;

public class BookManager {

    private ArrayList<BookDTO> booklist;
    private Scanner sc = new Scanner(System.in);

    public BookManager() {
        booklist = new ArrayList<>();
    }

    public void addBook(BookDTO book) {
        booklist.add(book);
    }

    public void deleteBook(int index) {

        if (index == -1) {
            System.out.println("삭제할 글이 존재하지 않음");

            return;
        }

        booklist.remove(index);
        System.out.println("성공적으로 삭제");

    }

    public int searchBook(String btitle) {

        for (int i = 0; i < booklist.size(); i++) {
            if (booklist.get(i).getTitle().contains(btitle)) {

                return i;
            }
        }

        return -1;
    }

    public void printBook(int index) {
        if (index == -1) {
            System.out.println("조회한 도서가 존재하지 않음");
        } else {
            System.out.println(booklist.get(index));
        }
    }

    public void displayAll() {
        int size = booklist.size();

        if (size == 0) {
            System.out.println("출력할 도서가 없습니다.");

            return;
        }

        for (Iterator<BookDTO> iterator = booklist.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next());
        }
    }

    public ArrayList<BookDTO> sortedBookList() {
        System.out.println("1. 카테고리 순으로 오름차순정렬");
        System.out.println("2. 카테고리 순으로 내림차순정렬");
        System.out.print("도서 정렬방식을 입력해주세요. : ");
        int num = sc.nextInt();

        Comparator<BookDTO> comparator = null;

        switch (num) {
            case 1 : comparator = new AscCategory(); break;
            case 2 : comparator = new DescCategory(); break;
            default : System.out.println("번호를 잘못 입력하였습니다."); return null;
        }
        booklist.sort(comparator);

        return booklist;
    }

    public void printBookList(ArrayList<BookDTO> printList) {

        if (printList == null) {

            return;
        }

        System.out.println();
        for (Iterator<BookDTO> iterator = booklist.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next());
        }
    }
}
