package com.ohgiraffers.hw2.comparator;

import java.util.Comparator;

import com.ohgiraffers.hw2.model.dto.BookDTO;

public class DescCategory implements Comparator<BookDTO>{

    @Override
    public int compare(BookDTO o1, BookDTO o2) {
        
        return o2.getCategory() - o1.getCategory();
    }

}
