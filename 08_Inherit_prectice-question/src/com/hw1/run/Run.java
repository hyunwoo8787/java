package com.hw1.run;

import java.util.Scanner;

import com.hw1.model.dto.Employee;
import com.hw1.model.dto.Person;
import com.hw1.model.dto.Student;

public class Run {
    public static void main(String[] args) {


        Person[] person = new Student[] {
                new Student("홍길동", 20, 178.2, 70.0, 1, "정보시스템공학과"),
                new Student("김말똥", 21, 187.3, 80.0, 2, "경영학과"),
                new Student("강개순", 23, 167.0, 45.0, 4, "정보통신공학과")
        };
        Scanner sc = new Scanner(System.in);

        for (Person student : person) {
            System.out.println(student.information());
        }

        Person[] em = new Person[10];
        int idx = 0;

        label:
        while (true) {
            System.out.print("이름을 입력해주세요 : ");
            String name = sc.next();
            System.out.print("나이를 입력해주세요 : ");
            int age = sc.nextInt();
            System.out.print("키를 입력해주세요 : ");
            double height = sc.nextDouble();
            System.out.print("몸무게를 입력해주세요 : ");
            double weight = sc.nextDouble();
            System.out.print("급여를 입력해주세요 : ");
            int salary = sc.nextInt();
            System.out.print("부서를 입력해주세요 : ");
            String dept = sc.next();

            em[idx] = new Employee(name, age, height, weight, salary, dept);

            while (true) {
                System.out.print("사원 정보를 추가하시겠습니까? (y/n) ");
                String tmp = sc.next();
                if (tmp.toUpperCase().charAt(0) == 'N' || idx == em.length) {
                    System.out.println("사원 추가를 중단합니다.");
                    break label;
                } else if (tmp.toUpperCase().charAt(0) == 'Y') {
                    System.out.println("사원 추가를 계속합니다.");
                    break;
                } else {
                    System.out.println("잘못된 문자를 입력하셨습니다. 다시 입력해주세요.");
                }
            }
            idx++;
        }

        for (int i = 0; i <= idx; i++) {
            System.out.println(em[i].information());
        }
        sc.close();
    }
}
