package com.greedy.section02.encapsulation.problem3;

public class Monster {

    String kinds;
    int hp;

    /**
     * <pre>
     * 몬스터의 정보를 입력받아서 몬스터 이름에 해당하는 필드 값을 변경해주는 메소드
     * </pre>
     * @param name
     */
    public void setName(String name) {
        this.kinds = name;
    }

    /**
     * <pre>
     * 몬스터의 체력정보를 입력받아서 몬스터의 체력에 해당하는 필드 값을 변경해주는 메소드
     * </pre>
     * @param hp
     */
    public void setHp(int hp) {

        if (hp > 0) {
            this.hp = hp;
        } else {
            this.hp = 0;
        }
    }

    /**
     * <pre>
     * 몬스터의 정보를 입력받아 모든 필드의 내용을 문자열로 되돌려주는 메소드
     * </pre>
     * @return 몬스터의 이름과 체력을 문자열로 치환하여 리턴해준다.
     */
    public String getInfo() {
        return "몬스터의 이름은 " + kinds + "이고, 체력은 " + hp + "입니다.";
    }
}
