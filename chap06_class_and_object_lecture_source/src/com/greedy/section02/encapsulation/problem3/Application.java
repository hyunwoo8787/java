package com.greedy.section02.encapsulation.problem3;

public class Application {

    public static void main(String[] args) {

        Monster monster1 = new Monster();
        monster1.setName("드라큘라");
        monster1.setHp(100);

        System.out.println(monster1.getInfo());

        Monster monster2 = new Monster();
        monster2.kinds = "뚜치";
        monster2.hp = 500 ;

        System.out.println("monster2 kinds : " + monster2.kinds);
        System.out.println("monster2 hp : " + monster2.hp);
    }
}
