package com.greedy.section01.user_type;

public class Member {

    /* 지금까지 클래스 내부에 메소드만 작성해봤다.
     * 하지만 클래스 내부에는 메소드를 작성하지 않고 바로 변수를 선언할 수도 있다.
     * 이것을 전역변수(필드 == 인스턴스 변수 == 속성)
     * */

    String[] hobby;
    String id;
    String pwd;
    String name;
    int age;
    char gender;

}
