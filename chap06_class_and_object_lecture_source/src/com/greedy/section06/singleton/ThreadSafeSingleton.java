package com.greedy.section06.singleton;

public class ThreadSafeSingleton {
    private static ThreadSafeSingleton instance;

    private ThreadSafeSingleton() {}

    /* 동시화 보장을 위해 메서드에 synchronized를 선언 */
    public static synchronized ThreadSafeSingleton getInstance() {
        /* instance 변수의 값이 null일 경우에만 인스턴스를 생성 */
        if (instance == null) {
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }
}
