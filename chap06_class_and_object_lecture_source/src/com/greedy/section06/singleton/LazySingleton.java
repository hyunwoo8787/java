package com.greedy.section06.singleton;

public class LazySingleton {

    /* 클래스가 초기화 되는 시점에서는 null로 초기화된다. */
    private static LazySingleton instance;

    private LazySingleton() {}

    /* 정적 변수의 값이 null일 경우에만 인스턴스를 생성한다. */
    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
