package com.greedy.section06.singleton;

public class StaticBlockSingleton {

    private final static StaticBlockSingleton instance;

    private StaticBlockSingleton() {}

    // static block를 사용하여 초기화
    static {
        try {
            instance = new StaticBlockSingleton();
        } catch (Exception e) {
            throw new RuntimeException("이미 StaticBlockSingleton이 생성되어 있습니다.");
        }
    }

    public static StaticBlockSingleton getInstance() {
        return instance;
    }
}
