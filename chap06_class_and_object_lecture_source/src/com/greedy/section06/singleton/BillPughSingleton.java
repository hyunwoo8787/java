package com.greedy.section06.singleton;

public class BillPughSingleton {
    private BillPughSingleton() {}

    /* Inner static Class로 인스턴스를 생성하여 동기화 문제를 해결 */
    private static class SingletonHelper {
        private static final BillPughSingleton SINGLETON = new BillPughSingleton();
    }

    /* Inner static Class에서 생성한 인스턴스를 리턴 */
    public static BillPughSingleton getInstance() {
        return SingletonHelper.SINGLETON;
    }
}
