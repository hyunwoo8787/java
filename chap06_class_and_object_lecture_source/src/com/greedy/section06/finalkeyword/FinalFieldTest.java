package com.greedy.section06.finalkeyword;

public class FinalFieldTest {

    public static void main(String[] args) {
        FinalFieldTest test = new FinalFieldTest("dsad");
    }
    /* final은 변경 불가의 의미를 가진다.
     *
     * 따라서 초기 인스턴스가 생성되고 나면 기본값이 0이 필드에 들어가게 되는데
     * 그 초기화 이후 값을 변경할 수 없기 때문에 선언하면서 바로 초기화를 해주어야 한다.
     * */

//    private final int nonStaticNum; // 0으로 초기화되어 이후 변경 불가능

    /* 해결할 수 있는 방법 2가지
     * 1. 선언과 동시에 초기화 한다.
     * 2. 생성자를 이용해서 초기화 한다. */
    private final int NON_STATIC_NUM = 1;

    private final String NON_STATIC_NAME;

    public FinalFieldTest(final String nonStaticName) {
        NON_STATIC_NAME = nonStaticName;
        System.out.println("생성자 호출");
    }

    /* static field에 final */
//    private static final int STATIC_NUM; // 0으로 초기화되어 이후 변경 불가능
    private static final int STATIC_NUM = 1;
    private static final double STATIC_DOUBLE;

    /* 생성자를 이용한 초기화는 불가능하다.
     * 생성자는 인스턴스가 생성되는 시점에 호출되기 때문에 그 전에는 초기화가 일어나지 못한다.
     * 하지만 static은 프로그램인 start될 때 할당되기 때문에 초기화가 되지 않은 상태로 선언된 것과 동일하여
     * 기본값으로 초기화 된 후 값을 변경하지 못하기 때문에 에러가 발생한다. */
    /* static final 변수는 생성자로 초기화 불가 */

    static {
        STATIC_DOUBLE = 5.0;
        System.out.println("STATIC 호출");
    }
    {
        System.out.println("호출");
    }
}
