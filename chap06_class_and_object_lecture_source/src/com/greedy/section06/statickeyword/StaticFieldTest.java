package com.greedy.section06.statickeyword;

public class StaticFieldTest {

    /* non-static 필드와 static 필드 선언 */
    private int nonStaticCount;
    private static int staticCount;

    public StaticFieldTest() {}

    public int getNonStaticCount() {

        return nonStaticCount;
    }

    public int getStaticCount() {

        /* static 필드에 접근하기 위해서는 클래스명.필드명으로 접근하다.
         * this.로도 접근은 가능하지만 this.을 사용하지 않는 것이 좋다.
         * */
        return staticCount;
    }

    /**
     * 두 필드 값을 1씩 증가시키기 위한 용도의 메소드
     * */
    public void increaseNonStaticCount() {
        nonStaticCount++;
    }

    public void increaseStaticCount() {
        staticCount++;
    }
}
