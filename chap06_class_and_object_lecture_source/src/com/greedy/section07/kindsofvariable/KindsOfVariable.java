package com.greedy.section07.kindsofvariable;

public class KindsOfVariable { // 클래스 영역의 시작

    /* 클래스 영역의 작성하는 변수를 필드라고 한다.
     * 필드 == 멤버변수(클래스가 가지는 멤버라는 의미) == 전역변수(클래스 내 전역에서 사용할 수 있는 변수라는 의미)
     * */

    /* non-static field, 인스턴스 변수 (인스턴스 생성 시점에 사용 가능한 변수라는 의미) */
    private int globalNum;

    /* static field, 클래스변수 (정적 영역에 생성되는 변수라는 의미) */
    private static int staticNum;

    public void testMethod(int num) { // 메소드 영역의 시작
        /* 메소드 영역에서 작성하는 변수를 지역변수라고 한다.
         * 메소드 괄호 안에 선언하는 변수를 매개변수라고 한다.
         * 매개변수도 일종의 지역변수로 생각하면 된다.
         * 지역변수와 매개변수 모두 메소드 호출 시 stack을 할당받아 stack에 생성된다.
         * */
        int localNum;

        System.out.println(num); // 매개변수를 호출 시 값이 넘어와서 변경되기 때문에 초기화가 필요 없다.

        /* 지역변수는 선언 외 다시 사용하기 위해서는 반드시 초기화가 되어야 한다. */
        localNum = 0;
        System.out.println(localNum);

        /* 멤버변수는 JVM이 기본값으로 초기화를 해준다. */
        System.out.println(globalNum);
        System.out.println(staticNum);

    } // 메소드 영역의 끝

    public void testMethod2() {
//        System.out.println(localNum); // 지역변수는 해당 지역(블럭 내)에서만 사용가능하다.
        System.out.println(globalNum);  // 전역변수는 다른 메소드에서도 사용가능하다.
        System.out.println(staticNum);

    }

} // 클래스 영역의 끝
