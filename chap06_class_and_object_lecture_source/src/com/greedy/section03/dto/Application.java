package com.greedy.section03.dto;

public class Application {
    public static void main(String[] args) {

        /* 행위 위주가 아닌 데이터를 하나로 뭉치기 위한 객체(Data Transfer Object)의 경우이다.
         * DTO -> 값을 받아서 다른곳으로 이동시키는 용도
         * VO -> 값 객체
         * 값 객체를 설계할 때는 행위가 아닌 데이터가 위주이며, 캡슐화의 원칙을 준수하여
         * 모든 필드를 private로 직접접근을 막고, 각 필드값을 변경하거나 반환하는 메소드를 세트로 미리 작성해둔다.
         *
         * private 필드와 필드값을 수정하는 설정자(setter), 필드에 접근하는 접근자(getter)들로 구성한다.
         * */

        MemberDTO member = new MemberDTO();
        member.setNumber(1);
        member.setName("홍길동");
        member.setAge(20);
        member.setGender('남');
        member.setHeight(180.5);
        member.setWeight(70.5);
        member.setActivated(true);

        System.out.println("회원번호\t\t: " + member.getNumber());
        System.out.println("회원명\t\t: " + member.getName());
        System.out.println("나이\t\t: " + member.getAge());
        System.out.println("성별\t\t: " + member.getGender());
        System.out.println("키\t\t: " + member.getHeight());
        System.out.println("몸무게\t\t: " + member.getWeight());
        System.out.println("회원활성상태\t: " + member.isActivated());
    }
}
