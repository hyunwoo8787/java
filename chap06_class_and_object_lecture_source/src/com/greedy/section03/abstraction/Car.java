package com.greedy.section03.abstraction;

public class Car {
    private int speed;
    private boolean isOn;

    /**
     * <pre>
     * 자동차의 시동을 걸기 위한 메소드
     * </pre>
     */
    public void turnOn() {
        if (isOn) {
            System.out.println("이미 시동이 걸려 있습니다.");
        } else {
            isOn = true;
            System.out.println("시동을 걸었습니다. 출발할 준비가 완료되었습니다.");
        }
    }

    /**
     * <pre>
     * 자동차를 가속시키기 위한 메소드
     * 시동이 걸린 상태인 경우에만 호출 시마다 10km/h씩 속드를 증가시킨다.
     * </pre>
     */
    public void accelator() {
        if (isOn) {
            speed += 10;
            System.out.println("자동차가 앞으로 움직입니다.");
            System.out.println("현재 차의 시속은 " + speed + "km/h 입니다");
        } else {
            System.out.println("시동이 걸려있지 않습니다. 먼저 시동을 걸어주세요.");
        }
    }

    /**
     * <pre>
     * 자동차를 멈추기 위한 메소드
     * 시동이 걸려있고 달리는 상태인 경우에만 멈출 수 있다.
     * </pre>
     */
    public void brake() {
        if (isOn) {
            if (speed != 0) {
                speed = 0;
                System.out.println("브레이크를 밟았습니다. 자동차를 멈춥니다.");
            } else {
                System.out.println("자동차가 이미 멈춰있는 상태입니다.");
            }
        } else {
            System.out.println("시동이 걸려있지 않습니다. 먼저 시동을 걸어주세요.");
        }
    }

    /**
     * <pre>
     * 자동차의 시동을 끄기 위한 메소드
     * </pre>
     */
    public void turnOff() {
        if (isOn) {
            if (speed != 0) {
                System.out.println("자동차가 움직이고 있어 시동을 끌 수 없습니다.");
            } else {
                isOn = false;
                System.out.println("시동을 끕니다. 다시 운행하시려면 시동을 켜주세요.");
            }
        } else {
            System.out.println("이미 시동이 꺼져 있는 상태입니다.");
        }
    }
}
