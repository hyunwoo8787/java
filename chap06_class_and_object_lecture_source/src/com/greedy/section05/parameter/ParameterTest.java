package com.greedy.section05.parameter;
public class ParameterTest {

    public void testPrimaryTypeParameter(int num) {
        System.out.println("매개변수로 전달받은 값 : " + num);
    }

    public void testPrimaryTypeArrayParameter(int[] arr) {

        /* 배열의 주소가 전달된다.
         * 즉, 인자로 전달하는 배열과 매개변수로 전달받은 배열은 서로 동일한 배열을 가리킨다. (얉은 복사)
         * */
        System.out.print("매개변수로 전달받은 값 : ");
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void testClassTypeParameter(RectAngle rectAngle) {
        System.out.println("매개변수로 전달받은 값 : " + rectAngle);

        System.out.println("변경 전 사각형의 넓이와 둘레 =============");
        rectAngle.calcArea();
        rectAngle.calcRound();

        rectAngle.setWidth(100);
        rectAngle.setHeight(100);

        System.out.println("변경 후 사각형의 넓이와 둘레 ============");
        rectAngle.calcArea();
        rectAngle.calcRound();
    }

    public void testVariableLengthArrayParameter(String name, String... hobby) {
        System.out.println("이름 : " + name);
        System.out.println("최미의 개수 : " + hobby.length);
        System.out.print("취미 : ");
        for (String string : hobby) {
            System.out.print(string + " ");
        }
        System.out.println();
    }

//    public void testVariableLengthArrayParameter(String... hobby) {
//        System.out.println("최미의 개수 : " + hobby.length);
//        System.out.print("취미 : ");
//        for (String string : hobby) {
//            System.out.print(string + " ");
//        }
//        System.out.println();
//    }
}
