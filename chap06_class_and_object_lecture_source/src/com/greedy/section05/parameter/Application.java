package com.greedy.section05.parameter;

public class Application {
    public static void main(String[] args) {

        /* 메소드의 파라미터 선언부에는 다양한 종류의 값을 인자로 전달하며 호출할 수 있다.
         *
         * 매개변수(parameter)로 사용 가능한 자료형
         * 1. 기본자료형
         * 2. 기본자료형 배열
         * 3. 클래스자료형
         * 4. 클래스자료형 배열
         * 5. 가변인자
         * */
        ParameterTest pt = new ParameterTest();

        /* 1. 기본자료형을 매개변수로 전달 받는 메소드 호출 */
        /* 기본자료형 8가지(byte, short, int, long, char, float, double, boolean) 모두 가능하다. */
        int num = 20;

        pt.testPrimaryTypeParameter(num);

        /* 기본자료형은 인자로 전달하는 값과 매개변수로 전달하는 값과 자료형이 동일하다. */

        /* 2. 기본자료형 배열을 매개변수로 전달 받는 메소드 호출 */
        int[] arr = { 1, 2, 3, 4, 5, 6, 7};
        pt.testPrimaryTypeArrayParameter(arr);

        /* 3. 클래스 자료형 */
        RectAngle r1 = new RectAngle(12.5, 22.5);
        System.out.println("인자로 전달하는 값 : " + r1);
        pt.testClassTypeParameter(r1);

        r1.calcArea();
        r1.calcRound();

        /* 5. 가변인자
         * 인자로 전달하는 값의 개수가 정해지지 않은 경우 가변배열을 활용할 수 있다.
         * */
        String name = "홍길동";

        pt.testVariableLengthArrayParameter(name, "테니스", "배드민턴", "농구", "축구", "배구", "수영");

        /* 메소드를 오버로딩하고 다시 돌아오면 컴파일 에러가 발생하는데
         * 이는 둘 중 어떤 메소드를 호출하는 것인지에 대한 모호성이 발생했기 때문에 에러가 나는 것이다.
         *
         * 가변배열을 매개변수로 이용한 메소드는 모호성으로 인해 오버로딩 하지 않는 것이 좋다.
         * */
    }
}
