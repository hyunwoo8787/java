package com.greedy.section1.method;

public class Application1 {
	public static void main(String[] args) {
		
		/* 메소드란?
		 * 어떤 특정 작업을 수행하기 위한 명령문의 집합이라고 할 수 있다.
		 * */
		
		System.out.println("main() 시작됨...");
		
		/* 객체 생성
		 * 클래스명 사용할 이름 = new 클래스명 ();
		 * 
		 * 메소드 호출
		 * 사용할 이름.메소드명();
		 * */
		
		Application1 app = new Application1();
		app.methodB();
		
		Application1.methodA();
		
		System.out.println("main() 종료됨...");
		Application1.methodA();
	}
	
	public static void methodA() {
		System.out.println("methodA() 시작됨...");
		System.out.println("methodA() 종료됨...");
	}
	
	public void methodB() {
		System.out.println("methodB() 시작됨...");
		methodC();
		System.out.println("methodB() 종료됨...");
		
	}
	
	public void methodC() {
		System.out.println("methodC() 시작됨...");
		System.out.println("methodC() 종료됨...");
	}
}
