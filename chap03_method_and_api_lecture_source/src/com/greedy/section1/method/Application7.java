package com.greedy.section1.method;

public class Application7 {
	public static void main(String[] args) {
		
		/* 매개변수와 리턴값 복합 활용
		 * 매개변수도 존재하고 리턴값도 존재하는 메소드를 이용하여 간단한 계산기 만들기
		 * */
		
		/* 숫자 두 개를 매개변수로 입력 받아서 연산하는 메소드를 사칙연산 별로 추가해서 호출
		 * */
		
		int first = 20;
		int second = 10;
		
		Application7 app7 = new Application7();
		System.out.println("숫자를 더한 결과 : " + app7.plusTwoNumbers(first, second, 55550, 30254));
		System.out.println("숫자를 뺀 결과 : " + app7.minusTwoNumbers(first, second));
		System.out.println("숫자를 곱한 결과 : " + app7.multipleTwoNumbers(first, second));
		System.out.println("숫자를 나눈 결과 : " + app7.divideTwoNumbers(first, second));
		System.out.println("숫자를 나누고 남은 몫 : " + app7.modTwoNumbers(first, second));
	}

	public int plusTwoNumbers(int first, int... second) {
		int result = first;
		for (int num : second) {
			result += num;
		}
		return result;
	}

	public int minusTwoNumbers(int first, int second) {
		return first - second;
	}

	public int multipleTwoNumbers(int first, int second) {
		return first * second;
	}

	public int divideTwoNumbers(int first, int second) {
		return first / second;
	}

	public int modTwoNumbers(int first, int second) {
		return first % second;
	}

}
