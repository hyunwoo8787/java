package com.greedy.section1.method;

/**
 * <pre>
 * Class : Application4
 * Comment : 여러 개의 전달인자와 매개변수를 이용한 메서드 호출
 * History
 * 2023/03/31 처음 작성
 * 2023/04/01 메서드 추가
 * </pre>
 * @author 김희승
 * @version 1.0
 */
public class Application4 {
	public static void main(String[] args) {
		
		/* 여러 개의 전달인자를 이용한 메서드 호출 테스트 
		 * 1. 여러개의 매개변수를 가진 메서드 호출 */
		Application4 app4 = new Application4();
		app4.testMethod("홍길동", 20, '남');
		// app4.testMethod("홍길동", 20, '남'); // 메서드에서 설정한 매개변수와 순서가 다르면 호출하지 못한다.
		
		/* 2. 변수에 저장된 값을 전달하여 호출할 수 있다. */
		String name = "유관순";
		int age = 20;
		char gender = '여';
		
		app4.testMethod(name, age, gender);
	}
	
	/**
	 * <pre>
	 * 이름과 나이, 성별의 인자를 전달 받아 한번에 출력해주는 기능을 제공
	 * </pre>
	 * @param name		출력할 이름
	 * @param age		출력할 나이
	 * @param gender	출력할 성별, 성별은 변경되지 않을 것을 보장합니다.
	 */
	public void testMethod(String name, int age, final char gender) {
		
		System.out.println("당신의 이름은 " + name + "이고, 나이는 " + age + "세 이며, "
							+ "성별은 " + gender + "입니다.");
	}
}
