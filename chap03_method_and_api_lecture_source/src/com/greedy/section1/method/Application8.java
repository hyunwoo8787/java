package com.greedy.section1.method;

public class Application8 {
	public static void main(String[] args) {
		
		/* static 메소드 호출
		 * static이 있는 메소드나 non-static 메소드 둘다 메소드의 동작흐름은 동일하다.
		 * */
		
		/* 
		 * non-static 메소드를 호출할 때
		 * 클래스명 사용할이름 = new 클래스명();
		 * 사용할이름.메소드명();
		 * 
		 * static메소드를 호출할 때
		 * 클래스명.메소드명();
		 * */
		
		System.out.println("10과 20의 합 : " + Application8.sumTwoNumbers(10, 20));
		
		/* 동일한 클래스 내에서 작성된 static 메소드는 클래스명 생략이 가능하다.*/
		
		System.out.println("10과 20의 합 : " + sumTwoNumbers(10, 20));

	}
	
	public static int sumTwoNumbers(int first, int second) {
		return first + second;
	}
}

class Calc {
	static final int plusTwoNumbers(int a, int b) {
		return a + b;
	}
}
