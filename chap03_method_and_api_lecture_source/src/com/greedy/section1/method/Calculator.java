package com.greedy.section1.method;

public class Calculator {
	public int plusTwoNumbers(int first, int second) {
		return first - second;
	}

	public int minusTwoNumbers(int first, int second) {
		return first - second;
	}

	public int multipleTwoNumbers(int first, int second) {
		return first * second;
	}

	public int divideTwoNumbers(int first, int second) {
		return first / second;
	}

	public int modTwoNumbers(int first, int second) {
		return first % second;
	}
	
	/* 두 수가 동일한 조건은 제외하고
	 * 두 수를 비교하여 첫 번째 숫자가 크면 두 번쨰 숫자를 반환하고,
	 * 아니면 첫 번째 숫자를 반환
	 * */
	public int minNumberOf(int first, int second) {
		return (first > second) ? second : first;
	}

	/* 두 수가 동일한 조건은 제외하고
	 * 두 수를 비교하여 첫 번째 숫자가 크면 첫 번쨰 숫자를 반환하고,
	 * 아니면 두 번째 숫자를 반환
	 * */
	public static int maxNumberOf(int first, int second) {
		return (first > second) ? first : second;
	}
}
