package com.greedy.section3.api.math;

public class Application1 {
	
	public static void main(String[] args) {
		
		/* API란?
		 * Application Programming Interface의 약자로 응용 프로그램에서 사용할 수 있도록,
		 * 운영체제나 프로그래밍 언어가 제공하는 기능을 제어할 수 있도록 만들어진 인터페이스를 뜻한다.
		 * 우리가 구현할 수 없거나 구현하기 번거로운 기능을 JDK를 설치하면 쉽게 사용할 수 있도록한 소스코드(클래스나 인터페이스) 들을 의미한다.
		 * */
		
		/* java.lang.Math
		 * Math 클래스는 수학에서 자주 사용하는 상수들과 함수들을 미리 구현해놓은 클래스이다.
		 * static method로만 구성되어 있다.
		 * */
		
		// 절대값 출력
		System.out.println("-7의 절대값 : " + (java.lang.Math.abs(-7)));

		
		/* 자주 사용하는 패키지라서 import하지 않고 사용할 수 있도록 해놓았다. 
		 * 그 이유는 컴파일러가 import java.lang.*; 이 코드를 자동으로 추가해서 컴파일을 하기 때문이다.
		 * */
		System.out.println("-7의 절대값 : " + (Math.abs(-5)));
		
		System.out.println("10과 20중 더 작은 것은 : " + Math.min(10, 20));
		System.out.println("20과 30중 더 큰 것은 : " + Math.max(20, 30));
		
		System.out.println("원주율 : " + Math.PI);
		
		System.out.println("난수발생 : " + Math.random());
	}
}
