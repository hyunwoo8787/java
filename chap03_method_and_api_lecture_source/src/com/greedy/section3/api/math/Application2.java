package com.greedy.section3.api.math;

public class Application2 {

	public static void main(String[] args) {
		
		/* 난수의 활용
		 * Math.random()을 이용해 발생한 난수는 0.0부터 1.0전의 실수 범위의 난수값을 반환한다.
		 * 
		 * 필요에 따라 정수 형태의 값을 원하는 범위만큼 발생시켜야 하는 경우들이 존재하는데
		 * 필요한 범위까지의 남수를 발생시켜보자
		 * */
		
		/* 0 ~ 9까지의 난수 발생 */
		int num = (int)(Math.random() * 10);
		System.out.println(num);
		
		/* 10 ~ 15까지의 난수 발생*/
		int random1 = (int)(Math.random() * 6 + 10);
		System.out.println(random1);
		
		/* -128 ~ 127까지의 난수 발생 */
		int random2 = (int)(Math.random() * 256 - 128);
		System.out.println(random2);
	}
}
