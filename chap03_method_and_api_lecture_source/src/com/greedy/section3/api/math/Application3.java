package com.greedy.section3.api.math;

import java.util.Random;

public class Application3 {

	public static void main(String[] args) {
		
		/* java.util.Random 클래스
		 * java.util.Random 클래스의 nextInt() 메소드를 이용한 난수 발생
		 * */
		
		/* 원하는 범위의 난수를 구하는 공식
		 * reandom.nextInt(구하려는 난수의 갯수) + 구하려는 난수의 최소값
		 * */
		
		Random random = new Random();

		/* 0부터 9까지 난수 발생 */
		int randomNumber1 = random.nextInt(10);
		System.out.println("0부터 9까지의 난수 : " + randomNumber1);
		
		/* 1부터 10까지 난수 발생 */
		int randomNumber2 = random.nextInt(10) + 1;
		System.out.println("1부터 10까지의 난수 : " + randomNumber2);
		
		/* 20부터 45까지의 난수 발생 */
		int randomNumber3 = random.nextInt(26) + 20;
		System.out.println("20부터 45까지의 난수 : " + randomNumber3);
	}
}
