package com.greedy.section3.api.scanner;

import java.util.Scanner;

public class Application2 {

	public static void main(String[] args) {
		
		/* Scanner의 nextLine()와ㅑ next() 
		 * nextLine() : 공백을 포함한 한 줄을 개행문자 전까지 읽어서 문자열로 반환.
		 * next() : 공백문자나 개행문자 전까지 읽어서 문자열로 반환.
		 * */
		
		/* 1. Scanner 객체 생성 */
		Scanner sc = new Scanner(System.in);
		
		/* 2. 문자열 입력 */
		/* 2-1. nextLine() */
		System.out.print("인사말을 입력해주세요 : ");
		String greeting1 = sc.nextLine();
		System.out.println(greeting1);
		
		char in = sc.nextLine().charAt(0);
		System.out.println(in);
		
		/* 2-2 next() */
		System.out.print("인사말을 입력해주세요 : ");
		String greeting2 = sc.next();
		System.out.println(greeting2);
		
		String greeting3 = sc.next();
		System.out.println(greeting3);
		
		sc.close();
		
	}
}
