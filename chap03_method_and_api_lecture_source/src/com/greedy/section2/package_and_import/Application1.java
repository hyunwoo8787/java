package com.greedy.section2.package_and_import;

public class Application1 {

	public static void main(String[] args) {
		com.greedy.section1.method.Calculator calc =
					new com.greedy.section1.method.Calculator();
		
		int min = calc.minNumberOf(30, 20);
		System.out.println(min);
		
		/* static 메서드인 경우 */
		int max = com.greedy.section1.method.Calculator.maxNumberOf(30, 20);
		System.out.println(max);
	}
}
