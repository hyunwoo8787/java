package com.greedy.section2.package_and_import;

/* 사용하려는 클래스까지를 작성 */
import com.greedy.section1.method.Calculator;

/* static import의 경우 사용하려는 static method까지 전부 써줘야한다. */
import static com.greedy.section1.method.Calculator.maxNumberOf;

public class Application2 {

	public static void main(String[] args) {
		// ctrl + shift + o : import 자동추가
		Calculator calc = new Calculator();
		int min = calc.minNumberOf(50, 60);
		System.out.println(min);

		int max = Calculator.maxNumberOf(50, 60);		
		System.out.println(max);
		
		int max2 = maxNumberOf(100, 200);
		System.out.println(max2);
	}
	
}
