package com.greedy.level01.normal;

public class Application1 {

	/* 실행용 메소드 */
	public static void main(String[] args) {
		
		/* RamdomMaker 클래스의 메소드를 호출해서 실행 */
		System.out.println(RandomMaker.rockPaperScissors());
		
		System.out.println(RandomMaker.tossCoin());
	}
}
