package com.ohgiraffers.hw3.controller;

import java.util.Scanner;

import com.ohgiraffers.hw3.model.vo.ProductDTO;

public class ProductController {
    private ProductDTO[] pro = null;
    public static int count;
    Scanner sc = new Scanner(System.in);

    {
        pro = new ProductDTO[10];
    }

    public void mainMenu() {
        int no = 0;

        do {
            System.out.println("===== 제품 관리 메뉴 =====");
            System.out.println("1. 제품 정보 추가");
            System.out.println("2. 제품 전체 조회");
            System.out.println("9. 프로그램 종료");
            System.out.print("번호를 입력해주세요 : ");

            no = sc.nextInt();
            switch (no) {
                case 1: productInput(); break;
                case 2: productPrint(); break;
                case 9: System.out.println("프로그램을 종료합니다."); break;
                default: System.out.println("잘못된 번호를 입력하셨습니다."); break;
            }
        } while (no != 9);
    }

    public void productInput() {
        System.out.println("===== 제품 추가 =====");
        System.out.print("제품 번호를 입력해주세요 : ");
        int pld = sc.nextInt();
        System.out.print("제품명을 입력해주세요 : ");
        String pName = sc.next();
        System.out.print("제품 가격을 입력해주세요 : ");
        int price = sc.nextInt();
        System.out.print("제품 세금을 입력해주세요 : ");
        double tax = sc.nextDouble();

        pro[count] = new ProductDTO(pld, pName, price, tax);
        System.out.println("제품 추가가 완료되었습니다.");
    }

    public void productPrint() {
        System.out.println("===== 제품 목록 출력 =====");
        for (int i = 0; i < count; i++) {
            System.out.println(pro[i].information());
            System.out.println();
        }
    }
}
