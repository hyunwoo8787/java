package com.ohgiraffers.hw3.model.vo;

import com.ohgiraffers.hw3.controller.ProductController;

public class ProductDTO {

    private int pld;
    private String pName;
    private int price;
    private double tax;

    public ProductDTO() {
        ProductController.count++;
    }

    public ProductDTO(int pid, String pName, int price, double tax) {
        this.pld = pid;
        this.pName = pName;
        this.price = price;
        this.tax = tax;
        ProductController.count++;
    }

    public String information() {

        return "제품 번호 : " + pld
            + "\n제품명 : " + pName
            + "\n제품 가격 : " + price
            + "\n제품 세금 : " + tax;
    }
}
