package com.ohgiraffers.hw1.run;

import com.ohgiraffers.hw1.model.dto.EmployeeDTO;

public class Application {

    public static void main(String[] args) {

        String[] employeeName = { "김말똥", "홍길동", "강말순" };
        int[] employeeSalary = { 3_000_000, 4_000_000, 1_000_000 };
        double[] employeeBonusPoint = { 0.2, 0.3, 0.01 };

        EmployeeDTO[] employeeDTOs = { new EmployeeDTO(),
                    new EmployeeDTO(1, employeeName[1], 19, 'M', "01022223333", "서울 잠실"),
                    new EmployeeDTO(2, employeeName[2], "교육부", "강사", 20, 'F', employeeSalary[2], employeeBonusPoint[2], "01011112222", "서울 마곡")
        };
        int average = 0;

        for (EmployeeDTO employeeDTO : employeeDTOs) {
            System.out.println(employeeDTO.information());
            System.out.println();
        }

        System.out.println("===============================================");

        employeeDTOs[0] = new EmployeeDTO(0, employeeName[0], "영업부", "팀장", 30, 'M', employeeSalary[0], employeeBonusPoint[0], "01055559999", "전라도 광주");
        employeeDTOs[1] = new EmployeeDTO(1, employeeName[1], "기획부", "부장", 19, 'M', employeeSalary[1], employeeBonusPoint[1], "01022223333", "서울 잠실");

        for (int i = 0; i < employeeDTOs.length - 1; i++) {
            System.out.println(employeeDTOs[i].information());
            System.out.println();
        }

        System.out.println("===============================================");

        for (int i = 0; i < employeeDTOs.length; i++) {
            int tmp = (int) (employeeSalary[i] * 12 * (1 + employeeBonusPoint[i]));
            System.out.println(employeeName[i] + "의 연봉 : " + tmp + "원");
            average += tmp;
        }

        System.out.println("===============================================");
        System.out.println("직원들의 연봉의 평균 : " + (average / employeeDTOs.length) + "원");

    }
}
