package com.ohgiraffers.hw2.run;

import java.util.Scanner;

import com.ohgiraffers.hw2.model.dto.StudentDTO;

public class Application {

    public static void main(String[] args) {

        StudentDTO[] students = new StudentDTO[10];
        String[] name = new String[10];
        int[] kor = new int[10];
        int[] eng = new int[10];
        int[] math = new int[10];
        Scanner sc = new Scanner(System.in);
        int idx = 0;

        label:
        while (students.length > idx) {
            System.out.print("학년을 입력해주세요 : ");
            int grade = sc.nextInt();
            System.out.print("반을 입력해주세요 : ");
            int classroom = sc.nextInt();
            System.out.print("이름을 입력해주세요 : ");
            name[idx] = sc.next();
            System.out.print("국어점수를 입력해주세요 : ");
            kor[idx] = sc.nextInt();
            System.out.print("영어점수를 입력해주세요 : ");
            eng[idx] = sc.nextInt();
            System.out.print("수학점수를 입력해주세요 : ");
            math[idx] = sc.nextInt();

            students[idx] = new StudentDTO(grade, classroom, name[idx], kor[idx], eng[idx], math[idx]);

            System.out.print("학생 정보를 추가하시겠습니까? (y/n) ");
            char ch = sc.next().charAt(0);

            switch (ch) {
                case 'Y':
                case 'y':
                    idx++;
                    break;
                default : System.out.println("입력을 종료합니다."); break label;
            }
        }

        for (int i = 0; i <= idx; i++) {
            System.out.println(name[i] + "의 평균점수 : " + ((kor[i] + eng[i] + math[i]) / 3));
        }

        for (int i = 0; i <= idx; i++) {
            System.out.println(students[i].information());
            System.out.println("평균점수 : " + ((kor[i] + eng[i] + math[i]) / 3));
        }

        sc.close();
    }
}
