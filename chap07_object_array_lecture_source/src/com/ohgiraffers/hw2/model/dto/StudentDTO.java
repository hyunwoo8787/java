package com.ohgiraffers.hw2.model.dto;

public class StudentDTO {
    private int grade;
    private int classroom;
    private String name;
    private int kor;
    private int eng;
    private int math;

    public StudentDTO() {
    }

    public StudentDTO(int grade, int classroom, String name, int kor, int eng, int math) {
        super();
        this.grade = grade;
        this.classroom = classroom;
        this.name = name;
        this.kor = kor;
        this.eng = eng;
        this.math = math;
    }

    public String information() {

        return "학년 : " + grade
            + "\n반 : " + classroom
            + "\n이름 : " + name
            + "\n국어점수 : " + kor
            + "\n영어점수 : " + eng
            + "\n수학점수 : " + math;
    }


}
