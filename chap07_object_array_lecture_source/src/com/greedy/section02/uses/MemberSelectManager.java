package com.greedy.section02.uses;

/**
 * <pre>
 * 회원 정보 조회를 담당하는 용도의 클래스
 * </pre>
 */
public class MemberSelectManager {

    public MemberDTO[] selectAllMembers() {

        /* 원래라면 데이터베이스나 파일에서 정보를 읽어와야겠지만 현재는 그런게 없으니 앞에서 작성한걸 이용 */
        return new MemberDTO[] {
                new MemberDTO(1, "user01", "pass01", "홍길동", 20, '남')
                , new MemberDTO(2, "user02", "pass02", "유관순", 16, '여')
                , new MemberDTO(3, "user03", "pass03", "이순신", 49, '남')
                , new MemberDTO(4, "user04", "pass04", "신사임당", 36, '여')
                , new MemberDTO(5, "user05", "pass05", "윤봉길", 22, '남')
        };
    }

}
