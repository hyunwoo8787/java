package com.greedy.section02.uses;

public class MemberInsertManager {

    /**
     * <pre>
     * 여러 명의 회원 정보를 한 번에 등록할 수 있는 기능 제공
     * </pre>
     * @param members 회원 정보를 객체 배열 형태로 전달
     */
    public void insert(MemberDTO[] members) {
        System.out.println("회원을 등록합니다.");

        for (MemberDTO memberDTO : members) {
            System.out.println(memberDTO.getName() + "님을 회원 등록에 성공했습니다.");
        }

        System.out.println("총 " + members.length + "명의 회원 등록에 성공했습니다.");
    }
}
