package com.greedy.section02.assignment_operator;

public class Application1 {
	public static void main(String[] args) {
		/* 대입연산자와 산술 복합 대입연산자
		 * '=' : 왼쪽의 피연산자에 오른쪽의 피연산자를 대입한다.
		 * '+=': 왼쪽의 피연산자에 오른쪽의 피연산자를 더한 결과를 왼쪽의 피연산자에 대입한다.
		 * '-=': 왼쪽의 피연산자에 오른쪽의 피연산자를 뺀 결과를 왼쪽의 피연산자에 대입한다.
		 * '*=': 왼쪽의 피연산자에 오른쪽의 피연산자를 곱한 결과를 왼쪽의 피연산자에 대입한다.
		 * '/=': 왼쪽의 피연산자에 오른쪽의 피연산자를 나눈 결과를 왼쪽의 피연산자에 대입한다.
		 * '%=': 왼쪽의 피연산자에 오른쪽의 피연산자를  결과를 왼쪽의 피연산자에 대입한다.
		 * */
	}
}
