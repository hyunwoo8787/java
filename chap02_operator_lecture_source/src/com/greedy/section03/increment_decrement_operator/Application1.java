package com.greedy.section03.increment_decrement_operator;

public class Application1 {
	public static void main(String[] args) {
		/* 증감연산자 
		 * 피연산자의 앞이나 뒤에 붙인다.
		 * '++' 피연산자의 값이 1 증가
		 * '--' 피연산자의 값이 1 감소
		 * */
		
		int first = 20;
		int result1 = first++ * 3;
		
		System.out.println("result1 : " + result1);
		System.out.println("first : " + first);
		
		int second = 20;
		int result2 = ++second * 3;
		
		System.out.println("result2 : " + result2);
		System.out.println("second : " + second);
		
		int num1 = 10;
		int num2 = 20;
		int num3 = 30;
		
		System.out.println(num1++);
		System.out.println(++num1 + num2++);
		System.out.println(num1++ + (--num2) + (--num3));
		// 10
		// 32
		// 61
		System.out.println(num1);// num1 = 13
		System.out.println(num2);// num2 = 20
		System.out.println(num3);// num3 = 29
	}
}
