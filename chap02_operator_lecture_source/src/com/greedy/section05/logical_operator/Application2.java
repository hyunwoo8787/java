package com.greedy.section05.logical_operator;

public class Application2 {
	public static void main(String[] args) {
		
		/* 논리 연산자의 우선순위와 활용
		 * 논리 and 연산자와 논리 or 연산자의 우선순위
		 * && : 11순위
		 * || : 12순위
		 * 논리 and 연산자 우선순위가 논리 or 연산자 우선순위보다 높다.
		 * */
		
		// 영문자인지 확인
		char word = '=';
		
		System.out.println("영문자 인지 확인 : " + ((word >= 'A' && word <= 'Z') || (word >= 'a' && word <= 'z')));
	}
}
