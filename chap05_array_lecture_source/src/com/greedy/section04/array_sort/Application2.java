package com.greedy.section04.array_sort;

public class Application2 {

    public static void main(String[] args) {
        
        /* 순차정렬
         * 
         * 순차정렬이란 정렬 알고리즘에서 가장 단순하고 기본이 되는 알고리즘
         * 배열의 처음과 끝을 탐색하면서 차순대로 정렬하는 가장 기초적인 정렬 알고리즘이다. */
        
        int[] arr = {2, 5, 4, 6, 1, 3};
        
        for (int i = 1; i < arr.length; i++) {
            
            for (int j = 0; j < i; j++) {
                
                /* 부등호의 방향이 '<' 오른차순 정렬
                 * 부등호의 방향이 '>' 내림차순 처리  */
                if (arr[i] < arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }
}
