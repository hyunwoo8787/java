package com.greedy.section04.array_sort;

public class Application4 {

    public static void main(String[] args) {
        /* 버블 정렬 (bubble sort) 
         * 인접한 두 개의 원소를 검사하여 정렬하는 방법
         * 구현이 쉽다는 장점이 있으며, 이미 정렬된 데이터를 정렬할 때 가장 빠르다.
         * 하지만 다른 정렬에 비해 정렬 속도가 느리며, 역순으로 정렬할 때 가장 느린 속도를 가진다.
         * */
        
        int[] arr = {2, 5, 9, 4, 8, 6, 1, 3, 7};
        int tmp;

        for (int i = 0; i < arr.length; i++) {
            for (int j = arr.length - 2; j >= i; j--) {
                
                if (arr[j] > arr[j + 1]) {
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }
}
