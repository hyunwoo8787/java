package com.greedy.section04.array_sort;

public class Application3 {

    public static void main(String[] args) {
        
        /* 선택정렬(selection sort) 
         * 
         * 배열을 전부 탐색하여 최소값을 고르고 왼쪽부터 채워나가는 방식의 정렬
         * 
         * 데이터의 양이 적을 때 좋은 성능을 나타낸다. (교환횟수가 적음)
         * 하지만 배열을 전부 탐색하여 최소값을 찾아야 하기 때문에 100개 이상의 자료에서는
         * 급격하게 속도가 저하된다. */
        
        int[] arr = {2, 5, 4, 6, 1, 3};
        
        int tmp;
        
        for (int i = 0; i < arr.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[min] > arr[j]) {
                    min = j;
                }
            }
            tmp = arr[min];
            arr[min] = arr[i];
            arr[i] = tmp;
        }
        
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }
}
