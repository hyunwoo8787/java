package com.greedy.section01.array;

public class Application5 {

    public static void main(String[] args) {
        /* 랜덤한 카드를 한 장 뽑아서 출력해보자 */
        
        String[] shapes = {"SPADE", "CLOVER", "HEART", "DIAMOND"};
        String[] cardNumbers = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        
        int randomShapeIndex = (int)(Math.random() * shapes.length);
        int randomCardNumberIndex = (int)(Math.random() * cardNumbers.length);
        
        System.out.println("당신이 뽑은 카드는 " + shapes[randomShapeIndex] + " " + cardNumbers[randomCardNumberIndex] + "입니다.");
    }
}
