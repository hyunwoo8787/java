package com.greedy.section01.array;

import java.util.Scanner;

public class Application2 {

    public static void main(String[] args) {
        
        /* 배열의 사용 방법 
         * 배열의 선언
         * 자료형[] 변수명;
         * 자료형 변수명[]; 로 선언할 수 있다.
         * */
        
        /* 선언은 stack에 배열의 주소를 보관할 수 있는 공간을 만드는 것 */
        int[] iarr;
        char carr[];
        
        /* 선언한 레퍼런스 변수에 배열을 할당하여 대입할 수 있다. 
         * new 연산자는 heap 영역에 공간을 할당하고 할당된 공간의 주소값을 반환하는 연산자이다.
         * */
        
        /* 배열을 할당할 시에는 반드시 배열의 크기를 지정해주어야 한다. */
        // iarr = new int[]; // 크기를 지정해주지 않아서 에러 발생
        iarr = new int[5];
        carr = new char[10];
        
        /* 위의 선언과 할당을 동시에 할 수 있다. */
        int[] iarr2 = new int[5];
        char[] carr2 = new char[10];

        /* heap메모리라는 이름으로 접근하는 것이 아닌 주소로 접근하는 영역이다. */
        System.out.println("iarr2 : " + iarr2); // 16진수로 주소값 출력
        System.out.println("carr2 : " + carr2);
        
        /* hashCode() : 일반적으로 객체의 주소값을 10진수로 변환하여 생성한 객체의 고유한 정수값을 반환하다.
         * */
        
        System.out.println("iarr2 hashCode : " + iarr2.hashCode()); 
        System.out.println("carr2 hashCode : " + carr2.hashCode());
        
        System.out.println("iarr2의 길이 : " + iarr2.length);
        System.out.println("carr2의 길이 : " + carr2.length);
        
        Scanner sc = new Scanner(System.in);
        System.out.print("새로 할당할 배열의 길이를 입력하세요 : ");
        int size = sc.nextInt();
        
        double[] darr = new double[size];
        
        System.out.println("darr의 hashCode : " + darr.hashCode());
        System.out.println("darr의 길이 : " + darr.length);

        darr = new double[30];
        
        System.out.println("수정 후 darr의 hashCode : " + darr.hashCode());
        System.out.println("수정 후 darr의 길이 : " + darr.length);

        darr = null;
        
        /* NullPointerException 
         * 아무것도 참조하지 않고 null이라는 특수한 값을 참조하고 있는 경우 참조연산자를 사용할 때 발생하는 에러이다.
         * */
        System.out.println("수정 후 darr의 hashCode : " + darr.hashCode());
        System.out.println("수정 후 darr의 길이 : " + darr.length); // NullPointerException 에러 발생
        
    }
}
