package com.greedy.section01.array.level02.normal;

import java.util.Scanner;

public class Application2 {

	public static void main(String[] args) {
		
		/* 주민등록번호를 스캐너로 입력 받고 문자 배열로 저장한 뒤,
		 * 성별 자리 이후부터 *로 가려서 출력하세요
		 * 
		 * -- 입력 예시 --
		 * 주민등록번호를 입력하세요 : 990101-1234567
		 * 
		 * -- 출력 예시 --
		 * 990101-1******
		 */
		
	    Scanner sc = new Scanner(System.in);
	    System.out.print("주민등록번호를 입력하세요 : ");
	    String jumin = sc.nextLine();
	    boolean isFind = false;
	    int count = 0;
	    
	    for (int i = 0; i < jumin.length(); i++) {
	        if (numbericCheck(jumin.charAt(i))) {
	            ++count;
	        }
	        if ('-' == jumin.charAt(i)) {
                isFind = true;
            }
        }
	    
	    if (!isFind && count != 13) {
	        System.out.println("잘못된 주민등록번호 입니다.");
	        sc.close();
	        return;
	    }
	    
	    for (int i = 0; i < jumin.length(); i++) {
	        if (i < 8) {
	            System.out.print(jumin.charAt(i));
	        } else {
	            System.out.print("*");
	        }
	    }
	    sc.close();
	}
	
	public static boolean numbericCheck(char ch) {
	    if (ch >= 48 && ch <= 57) {
	        return true;
	    }
	    return false;
	}
}
