package com.greedy.section01.array;

public class Application3 {

    public static void main(String[] args) {
        /* 기본적으로 배열을 선언하고 할당하게 되면
         * 배열의 각 인덱스에는 자바에서 지정한 기본값으로 초기화가 된 상태가 된다.
         * heap영역에는 값이 없는 빈 공간이 존재할 수 없다.
         * */
        
        /* 값의 형태별 기본값
         * 정수 : 0
         * 실수 : 0.0
         * 논리 : false
         * 문자 : \u0000
         * 참조 : null <= String, 사용자정의타입
         * */
        
        int[] arr = new int[5];
        
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
        System.out.println(arr[3]);
        System.out.println(arr[4]);
        
        arr[0] = 10;
        for (int i = 0; i < arr.length; i++) {
            System.out.println("arr[" + i + "]의 값 : " + arr[i]);
        }

        /* 자바에서 지정한 기본값 외의 값으로 초기화를 하고 싶은 경우 블럭을 이용한다.
         * 블럭을 사용하는 경우에는 new를 사용하지 않아도 되며, 값의 갯수만큼 자동으로 크기를 설정한다.
         * */
        int[] arr2 = {11, 22, 33, 44, 55};
        int[] arr3 = new int[]{11, 22, 33, 44, 55};
        
        System.out.println("arr2의 길이 : " + arr2.length);
        System.out.println("arr3의 길이 : " + arr3.length);
        for (int i = 0; i < arr2.length; i++) {
            System.out.println("arr2[" + i + "]의 값 : " + arr2[i]);
        }

        String[] sarr = {"apple", "banana", "grape", "orange"};
        
        for (int i = 0; i < sarr.length; i++) {
            System.out.println("sarr[" + i + "]의 값 : " + sarr[i]);
        }
    }
}
