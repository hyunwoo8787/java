package com.greedy.section03.array_copy;

import java.util.Arrays;

public class Application3 {

    public static void main(String[] args) {

        /* 깊은 복사는 heap에 생성된 배열이 가지고 있는 값을
         * 또 다른 배열에 복사하는 것이다.
         * 
         * 서로 같은 값을 가지고 있지만 두 배열은 서로 다른 배열이기에
         * 하나의 배열에서 변경을 하더라도 다른 배열에는 영향을 주지 않는다 */

        /* 깊은 복사를 하는 방법은 4가지가 있다.
         * 1. for문을 이용한 동일한 인덱스의 값 복사
         * 2. Object의 clone()을 이용한 복사
         * 3. System의 arraycopy()를 이용한 복사
         * 4. Arrays의 copyOf()를 이용한 복사
         * 
         * 이 중에서 가장 높은 성능을 보이는 것은 순수 배열의 복수를 위해 만들어진 arraycopy()
         * 가장 많이 사용되는 방식은 좀더 유연한 방식인 copyOf() 메소드다.
         * 
         * clone()은 이전 배열과 같은 배열 밖에 만들 수 없다는 특징이 있다
         * 그 외 3가지 방법은 복사하는 배열의 길이를 마음대로 변경할 수 있다. */

        /* 원본 배열 할당 및 초기화 */
        int[] original = { 1, 2, 3, 4, 5 };
        
        print(original);
        
        /* 1. for문을 이용한 동일한 인덱스 값 복사 */
        int[] copyArr1 = new int[10];
        
        for (int i = 0; i < original.length; i++) {
            copyArr1[i] = original[i];
        }
        
        print(copyArr1);
        
        /* 2. Object의 clone()을 이용한 복사 */
        int[] copyArr2 = original.clone();
        print(copyArr2); // 동일한 길이, 동일한 값을 가지는 배열이 생성되어 복사며, 다른 주소를 가지고 있다.
        
        /* 3. System의 arraycopy()을 이용한 복사 */
        int[] copyArr3 = new int[10];
        
        /* 매개변수 손서의 의미 
         * 원본배열, 원본에서 시작할 인덱스, 복사본 배열, 복사를 시작할 인덱스, 복사할 길이 */
        System.arraycopy(original, 0, copyArr3, 3, original.length);
        print(copyArr3);
        
        /* 4. Arrays의 copyOf()을 이용한 복사 */
        int[] copyArr4 = Arrays.copyOf(original, 10);
        print(copyArr4);
        
        
    }

    /**
     * <pre>
     * 배열을 매개변수로 전달받아
     * 모든 인덱스에 저장되어 있는 값을 출력하는 기능을 제공
     * </pre>
     * 
     * @param arr 저장된 값을 출력하기 위한 정수 배열
     */
    public static void print(int[] arr) {
        System.out.println("arr의 hashCode : " + arr.hashCode());

        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
