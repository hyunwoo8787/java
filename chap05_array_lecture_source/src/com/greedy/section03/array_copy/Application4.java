package com.greedy.section03.array_copy;

public class Application4 {

    public static void main(String[] args) {

        /* 깊은 복사는 원본과 복사본 둘 중 한가지 값을 변경해도 다른 하나에 영향을 주지 않는다.
         * 같은 값을 가지고 있는 서로 다른 배열이기 때문이다. */

        /* 두 개의 같은 값을 가지고 배열 생성 */
        int[] arr = { 1, 2, 3, 4, 5 };
        int[] arr2 = arr.clone();
        
        for (int i = 0; i < arr.length; i++) {
            arr[i] += 10;
        }
        
        /* 향상된 for문 : jdk 1.5 버전부터 추가 
         * 
         * 배열 인덱스에 하나씩 차례로 접근해서 담긴 값을 임시로 사용할 변수에 담고 반복문을 실행한다. */
        
        for (int i : arr2) {
            i += 10;
        }

        Application3.print(arr);
        Application3.print(arr2);
        
        /* 주의 : 향상된 for문은 배열 인덱스에 차례로 접근할 때는 편하게 사용할 수 있지만 값을 변경할 수는 없다. */
        
        String[] str = {"홍길동", "유관순", "신사임당"};
        
        for (String string : str) {
            System.out.print(string);
            System.out.print(" ");
        }
    }
}
