package com.greedy.section03.array_copy;

public class Application2 {

    public static void main(String[] args) {
        /* 얕은 복사의 활용
         * 
         * 얕은복사를 활용하는 것을 주로 메소드 호출 시 인자로 사용하는 경우와
         * 리턴값으로 동일한 배열을 리턴해주고 싶은 경우에 사용한다.
         * */
        
        String[] names = {"홍길동", "유관순", "이순신"};
        
        System.out.println("names의 hashCode : " + names.hashCode());
        print(names);
        
        /* 리턴값으로 활용 */
        String[] newarr = getAnimals();
        System.out.println("newarr의 hashCode : " + newarr.hashCode());

        /* 다른 메소드에서 동일한 배열(객체)를 사용하도록 하고 싶은 경우 얕은복사를 이용 */
        
        System.out.println("=================================");
        print(newarr);
    }
    
    /**
     * <pre>
     * 배열을 매개변수로 전달받아 모든 인덱스에 저장되어 있는 값을 출력하는 기능을 제공
     * </pre>
     * @param str 저장된 값을 출력하기 위한 문자열의 배열 제공
     */
    public static void print(String[] str) {
        System.out.println("str의 hashCode : " + str.hashCode());
        
        for (String string : str) {
            System.out.println(string);
        }
    }
    
    /**
     * <pre>
     * 동물 종류로 생성된 문자열 배열을 반환하는 메소드
     * </pre>
     * @return 동물 종류가 담긴 문자열 배열을 반환한다.
     */
    public static String[] getAnimals() {
        
        String[] animals = {"낙타", "호랑이", "나무늘보"};
        
        System.out.println("animalss의 hashCode : " + animals.hashCode());

        return animals;
    }
}
