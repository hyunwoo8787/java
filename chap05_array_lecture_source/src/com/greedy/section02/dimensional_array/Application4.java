package com.greedy.section02.dimensional_array;

public class Application4 {

    public static void main(String[] args) {

        /*
         * 1차원 배열이나, 2차원 배열은 할당과 동시에 JVM이 기본값으로 초기화 해주는데
         * 따로 초기화하고 싶은 리터럴로 초기화를 진행할 수 있다.
         */
        int[][] arr = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };

        for (int[] is : arr) {
            for (int is2 : is) {
                System.out.print(is2 + "\t");
            }
            System.out.println();
        }

        int[][] arr2 = { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9, 10, 11, 12 } };
        for (int[] is : arr2) {
            for (int is2 : is) {
                System.out.print(is2 + "\t");
            }
            System.out.println();
        }
    }
}
