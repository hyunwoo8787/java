package com.greedy.section02.dimensional_array;

public class Application1 {

    public static void main(String[] args) {
     
        /* 다차원 배열
         * 
         * 다차원 배열은 2차원 이상의 배열을 의미한다.
         * 배열의 인덱스마다 또 다른 배열의 주소를 보관하는 배열을 의미한다.
         * 즉, 2차원 배열은 1차원 배열 여러개를 하나로 묶어서 관리하는 배열이다.
         * */
        
        /* 2차원 배열을 사용하는 방법
         * 1. 배열의 주소를 보관할 레퍼런스 변수 선언 (stack)
         * 2. 여러 개의 1차원 배열의 주소를 관리하는 배열을 생성 (heap)
         * 3. 각 인덱스에서 관리하는 배열을 할당 (heap)하여 주소를 보관하는 배열에 저장
         * 4. 생성한 여러 개의 1차원 배열에 차례로 접근해서 사용
         * */
        
        /* 1. 배열의 주소를 보관할 레퍼런스 변수 선언 (stack) */
        int[][] arr1;
        int arr2[][];
        int[] arr3[];
        
        /* 2. 여러 개의 1차원 배열의 주소를 관리하는 배열을 생성 (heap) */
        //arr1 = new int[][];     // 크기를 지정하지 않으면 에러
        //arr1 = new int[][4];    // 주소를 묶어서 관리할 배열의 크기를 지정하지 않으면 에러
        arr1 = new int[3][];
        
        /* 3. 각 인덱스에서 관리하는 배열을 할당 (heap)하여 주소를 보관하는 배열에 저장 */
        arr1[0] = new int[5];
        arr1[1] = new int[5];
        arr1[2] = new int[5];
        
        arr2 = new int[3][5];
        /* 참고 : 위에서 진행한 2, 3번을 동시에 진행할 수도 있다.
         * 
         * 앞 부분 정수는 주소를 관리하는 배열의 크기, 뒷 부분 정수는 각 인덱스에 할당하는 배열의 길이이다.
         * 
         * 관리하는 여러 개의 배열의 길이가 동일한 경우 (arr2 = new int[3][5];)와 같이 한번에 할당할 수 있다.
         * 
         * 하지만 여러 개의 배열의 길이가 서로 다른 경우에는 (arr1 = new int[3][];)와 같이 인덱스 별로 할당해주어야한다.
         * 
         * 서로 같은 길이의 여러 개 배열을 하나로 묶어서 관리하는 2차원 배열을 정변배열이라고 하며,
         * 서로 길이가 다른 여러 개의 배열을 하나로 묶어 관리하는 2차원 배열을 가변배열이라고 한다.
         * */
        
        /* 4. 생성한 여러 개의 1차원 배열에 차례로 접근해서 사용 */
        /* 할당 후에 아무 값을 대입하지 않아도 heap 영역에는 값이 없는 상태로
         * 공간을 생성할 수 없기 때문에 기본값이 들어가 있다.
         * */
        
        /*
         * for (int i = 0; i < arr1[0].length; i++) { System.out.print(arr1[0][i] +
         * " "); }
         * 
         * System.out.println(); for (int i = 0; i < arr1[1].length; i++) {
         * System.out.print(arr1[1][i] + " "); }
         * 
         * System.out.println(); for (int i = 0; i < arr1[2].length; i++) {
         * System.out.print(arr1[2][i] + " "); }
         */
        
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                System.out.print(arr1[i][j] + " ");
            }
            System.out.println();
        }
        
        
    }
}
