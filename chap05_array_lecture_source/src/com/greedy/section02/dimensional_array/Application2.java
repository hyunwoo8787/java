package com.greedy.section02.dimensional_array;

public class Application2 {
    public static void main(String[] args) {
        
        /* 2차원 정변배열을 선언 및 할당 후 값을 대입하고 출력하기 */
        
        /* 1. 배열의 선언 및 할당
         * 정변 배열의 경우 각 인덱스별 배열을 따로 할당할 수 있지만
         * 선언과 동시에 모든 배열을 할당할 수도 있다.
         * 자료형[][] 변수명 = new 자료형[할당할 배열의 개수][할당할 배열의 길이];
         * */
        
        int[][] arr = new int[3][5];
        
        /* 2. 각 배열의 인덱스에 접근해서 값 대입 후 출력 */
        /* 값 대입 (수동) */
        arr[0][0] = 1;
        arr[0][1] = 2;
        arr[0][2] = 3;
        arr[0][3] = 4;
        arr[0][4] = 5;

        arr[1][0] = 6;
        arr[1][1] = 7;
        arr[1][2] = 8;
        arr[1][3] = 9;
        arr[1][4] = 10;
        
        arr[2][0] = 11;
        arr[2][1] = 12;
        arr[2][2] = 13;
        arr[2][3] = 14;
        arr[2][4] = 15;
        
        /* 값 출력 */
        System.out.print(arr[0][0] + "\t");
        System.out.print(arr[0][1] + "\t");
        System.out.print(arr[0][2] + "\t");
        System.out.print(arr[0][3] + "\t");
        System.out.print(arr[0][4] + "\t");
        
        System.out.println();
        
        System.out.print(arr[1][0] + "\t");
        System.out.print(arr[1][1] + "\t");
        System.out.print(arr[1][2] + "\t");
        System.out.print(arr[1][3] + "\t");
        System.out.print(arr[1][4] + "\t");
        
        System.out.println();
        
        System.out.print(arr[2][0] + "\t");
        System.out.print(arr[2][1] + "\t");
        System.out.print(arr[2][2] + "\t");
        System.out.print(arr[2][3] + "\t");
        System.out.print(arr[2][4] + "\t");
        
        System.out.println();
        System.out.println("===============================");
        
        int value = 1;
        
        
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = value++;
            }
        }
        
        for (int[] is : arr) {
            for (int is2 : is) {
                System.out.print(is2 + "\t");
            }
            System.out.println();
        }
    }
}
