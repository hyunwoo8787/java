package com.greedy.section02.userexception;

import com.greedy.section02.userexception.exception.MoneyNegativeException;
import com.greedy.section02.userexception.exception.NotEnoughMoneyException;
import com.greedy.section02.userexception.exception.PriceNegativeException;

public class Application3 {

    public static void main(String[] args) {

        /* multi-catch
         * JDK 1.7에서 추가된 구문
         * 동일한 레벨의 다른 타입의 예외를 하나의 catchㅂ르럭으로 다룰 수 있다. */

        ExceptionTest et = new ExceptionTest();

        try {
            et.checkEnoughtMoney(20000, 10000);
        } catch (PriceNegativeException | MoneyNegativeException e) {

            /* e.getClass()로 발생한 예외클래스의 이름을 알 수 있다. */
            System.out.println(e.getClass().getSimpleName() + " 발생!");
            /* e.getMessage()로 발생한 예외 메세지를 알 수 있다. */
            System.out.println(e.getMessage());
        } catch (NotEnoughMoneyException e) {

            /* 예외 클래스명, 예외발생 위치, 예외 메세지 등을 stack호출 역순으로
             * 빨간색 글씨를 이용해서 로그 형태로 출력해주는 기능 */
            e.printStackTrace();
        } finally {

            System.out.println("finally 블럭의 내용이 동작함.");
        }

        System.out.println("프로그램을 종료합니다.");
    }
}
