package com.greedy.section02.userexception.exception;

public class NegativeException extends Exception {

    /* 기본생성자 */
    public NegativeException() {}

    /* 매개변수 있는 생성자 */
    public NegativeException(String message) {
        super(message);
    }
}
