package com.greedy.section02.userexception.exception;

/**
 * 상품 가격이 음수일 경우에 띄워줄 예외 클래스
 */
public class PriceNegativeException extends NegativeException {

    public PriceNegativeException() {}

    public PriceNegativeException(String message) {
        super(message);
    }
}
