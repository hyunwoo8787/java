package com.greedy.section02.userexception.exception;

/**
 * 보유하고 있는 돈이 음수일 경우 예외처리하기 위한 클래스
 */
public class MoneyNegativeException extends NegativeException {

    public MoneyNegativeException() {}

    public MoneyNegativeException(String message) {
        super(message);
    }
}
