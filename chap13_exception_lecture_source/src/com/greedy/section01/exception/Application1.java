package com.greedy.section01.exception;

public class Application1 {

    public static void main(String[] args) throws Exception {

        /* 예외처리
         *
         * 자바 프로그램 작성 시 자바 문법에 맞지 않는 경우
         * 코드를 컴파일 하려고 할 때 컴파일 에러를 발생시킨다. 엄밀히 말하면 문법상의 오류(Syntax Error)이다.
         * 혹은 자바 문법에 맞게 작성하여 컴파일에 문제가 없더라도 실행하면서 예상치 못하게 오류가 발생할 수 있다.
         *
         * 오류(Error)
         * 시스템 상에서 프로그램에 심각한 문제를 발생하여 실행중인 프로그램이 종료되는 것을 말한다.
         * 이러한 오류는 개발자가 미리 예측하고 처리하는 것이 불가능하며, 오류에 대한 처리는 할 수 없다.
         *
         * 예외(Exception)
         * 오류와 마찬가지로 실행중인 프로그램을 비정상적으로 종료시키지면
         * 발생할 수 있는 경우를 미리 예측하고 처리할 수 있는 미약한 오류를 말한다.
         * 개발자는 이러한 예외에 대해 예외처리를 통해 예외 상황을 적절히 처리하여 코드의 흐름을 컨트롤 할 수 있다. */

        ExceptionTest test = new ExceptionTest();

        /* throws로 위임 */
        test.checkEnoughMoney(700, 600); // 예외 발생 구문 이하 구문은 동작하지 않고 되돌아 온다.
                                         // 하지만 메인메소드 또한 예외를 처리하지 않고 위임했기 때문에
                                         // 프로그램은 비정상적으로 종료되고 아래 구문은 출력되지 않는다.

        System.out.println("프로그램을 종료합니다.");
    }
}
