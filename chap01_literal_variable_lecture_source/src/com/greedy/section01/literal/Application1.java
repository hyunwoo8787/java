package com.greedy.section01.literal;

public class Application1 {

	public static void main(String[] args) {

		// 1. 숫자 형태의 값
		// 	1-1. 정수 형태의 값
		System.out.println(123);
		// 	1-2. 실수 형태의 값
		System.out.println(1.23);
		// 2. 문자 형태의 값
		System.out.println('a'); // 문자 형태의 값은 홀따옴표(single-quotation)로 감싸야한다
		System.out.println('1'); // 숫자 1이 아닌 문자 1로 인식한다
		// System.out.println('ab'); // 두 개 이상은 문자로 취급하지 않기 떄문에 오류가 발생한다.
		// System.out.println(''); // 아무 문자도 기록하지 않는 경우에도 오류가 발생한다.
		// 3. 문자열 형태의 값
		System.out.println("안녕하세요;"); // 문자열은 문자 여러 개가 나열된 형태를 말하며 쌍따옴표(double-quotation)로 감싸야한다
		System.out.println("123"); // 정수이지만 쌍따옴표로 감싸져있기 때문에 문자열로 보아야 한다.
		System.out.println(""); // 아무 값도 없는 빈 쌍따옴표도 문자열로 취급한다.
		System.out.println("a"); // 한 개의 문자도 쌍따옴표로 감싸면 문자열이다. (문자 a와는 다르다)
		// 4. 논리 형태의 값
		System.out.println(false); // 논리 값은 true(참)과 false(거짓)로 나타낸다
		System.out.println(true);

	}
}
