package com.greedy.section01.literal;

public class Application2 {
	/*
	 * public static void main(String[] args) { } 는 실행용 메소드라는 이름으로
	 * 각 클래스마다 하나씩 존재하며 클래스를 실행했을 때 맨처음 실행되는 메소드를 말한다.
	 */
	public static void main(String[] args) {

		/*
		 * 값을 직접 연산하여 출력 
		 * 연산자의 종류 ( + , - , * , / , % )에 따라 연산의 결과가 달라진다.
		 */
		System.out.println("============ 정수와 정수 연산 ============");
		System.out.println(123 + 5467);
		System.out.println(123 - 23);
		System.out.println(123 * 10);
		System.out.println(123 / 10);
		System.out.println(123 % 10);
		
		System.out.println("============ 실수와 실수 연산 ============");
		System.out.println(1.23 + 1.23);
		System.out.println(1.23 - 0.23);
		System.out.println(1.23 * 10.0);
		System.out.println(1.23 / 2.0);
		System.out.println(1.23 % 1.0);
	
		/* 
		 * 정수와 실수의 연산도 수학에서 사용하는 사칙연산에 나머지를 구하는 연산을 사용할 수 있고,
		 * 정수와 실수의 연산의 결과는 항상 실수가 나온다.
		 * */
		System.out.println("============ 정수와 실수 연산 ============");
		System.out.println(123 + 0.5);
		System.out.println(123 - 0.5);
		System.out.println(123 * 0.5);
		System.out.println(123 / 0.5);
		System.out.println(123 % 0.5);

		/* 
		 * 문자끼리의 연산도 사칙연산 ( - , + , * , /)과 mod 연산( % )이 가능하다.
		 * */
		System.out.println("============ 문자와 문자 연산 ============");
		System.out.println('a' + 'z');
		System.out.println('a' - 'z');
		System.out.println('a' * 'z');
		System.out.println('a' / 'z');
		System.out.println('a' % 'z');
		
		System.out.println("============ 문자와 정수 연산 ============");
		System.out.println('a' + 2147483627);
		System.out.println('a' - 1);
		System.out.println('a' * 2);
		System.out.println('a' / 2);
		System.out.println('a' % 2);
		
		System.out.println("============ 문자와 실수 연산 ============");
		System.out.println('a' + 1.0);
		System.out.println('a' - 1.0);
		System.out.println('a' * 2.0);
		System.out.println('a' / 2.0);
		System.out.println('a' % 2.0);

		/* 문자는 내부적으로 숫자 취급을 한다.
		 * 지금까지 연산은 숫자 끼리의 연산을 한 것이고, 숫자(정수 혹은 실수) 형태의 값은
		 * 수학의 사칙연산과 mod연산이 가능하다.
		 * */
		
		/* 문자열과 다른 형태의 값 연산
		 * -> 문자열과의 연산은 '+' 연산만 가능하다.
		 * -> 문자열과 문자열의 연산도 '+' 연산만 가능하다.
		 * */
		
		System.out.println("========== 문자열과 문자열의 연산 =========");
		System.out.println("Hello" + " " + "World");
		// System.out.println("Hello" - "World"); // 오류
		// System.out.println("Hello" * "World"); // 오류
		// System.out.println("Hello" / "World"); // 오류
		// System.out.println("Hello" % "World"); // 오류
		System.out.println("Hello" + 13423421);
		System.out.println("Hello" + 1342.3421);
		System.out.println("Hello" + 'a');
		System.out.println("Hello" + true);
		
		/* 논리값 연산
		 * 논리값과 다른 자료형의 연산은 정수, 실수, 문자 모두 연산자 사용이 불가능하다
		 * 
		 * 단, 논리값과 문자열의 '+' 연산만 가능하다.
		 * */
		// System.out.println(true + false); // 오류
		// System.out.println(true + 1.0); // 오류
		// System.out.println(true + 'a'); // 오류
		System.out.println("" + true);
	}
}
