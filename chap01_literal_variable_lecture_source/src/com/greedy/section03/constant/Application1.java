package com.greedy.section03.constant;

public class Application1 {
	public static void main(String[] args) {
		/* 상수 선언
		 * 자료형 앞에 final 키워드를 붙인다. */
		final int AGE;
		AGE = 100;
		
		System.out.println(AGE);
		System.out.println(AGE * 2);

		// AGE = 20; // 상수는 한번 초기화를 하고 나면 값을 수정할 수 없다
		
		int sum = AGE;
		
		/* 상수의 명명 규칙
		 * 변수의 명명 규칙과 컴파일 에러를 발생시키는 규칙은 동일
		 * 단, 개발자들끼리의 암묵적인 규칙에서 일부 차이를 보인다.
		 * 
		 *  1. 모든 문자는 영문자 대문자 혹은 숫자만 사용한다.
		 *  2. 단어와 단어 연결은 언더스코어(_)를 사용한다.
		 *  */
		
		final int AGE1 = 20;
		final int AGE2 = 30;
		final int age1 = 40; // 소문자로도 사용은 가능하지만 변수와 구분하기 어렵기 떄문에 만들어진 규칙
		final int HUMAN_MAX_AGE = 100;
		final String USER_NAME = "홍길동";
	}
}
