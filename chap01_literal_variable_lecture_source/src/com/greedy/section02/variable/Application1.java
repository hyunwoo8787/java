package com.greedy.section02.variable;

import java.lang.annotation.Retention;
import java.security.Identity;
import java.util.IdentityHashMap;

public class Application1 {
	public static void main(String[] args) {

		/*
		 * 변수의 사용 목적 
		 * 1. 값에 의미를 부여하기 위한 목적 (의미전달을 쉽게 하여 협업 및 유지보수에 유리해짐) 
		 * 2. 한 번 저장해둔 값을 재사용 하기 위한 목적 
		 * 3. 시간에 따라 변하는 값을 저장하고 사용할 수 있다. (변하는 값을 저장하기 위한 공간)
		 */
		
		System.out.println("============= 값에 의미 부여 테스트 ================");
		System.out.println("보너스를 포함한 급여 : " + (1_000_000 + 200_000) + "원");
		
		// 이대로 출력하면 100만원과 20만원 중에 어떤 금액이 보너스인지 알 수 없기 때문에
		// 의미를 부여하기 위해 변수를 사용한다.
		
		int salary = 1_000_000;
		int bonus = 200_000;
		System.out.println("보너스를 포함한 급여 : " + (salary + bonus) + "원");
		
		System.out.println("============ 변수에 저장한 값 재사용 테스트 ===========");
		System.out.println("1번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("2번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("3번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("4번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("5번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("6번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("7번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("8번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("9번 고객에게 포인트를 100포인트를 지급하였습니다.");
		System.out.println("10번 고객에게 포인트를 100포인트를 지급하였습니다.");
		
		int point = 500;
		System.out.println("1번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("2번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("3번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("4번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("5번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("6번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("7번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("8번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("9번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		System.out.println("10번 고객에게 포인트를 " + point + "포인트를 지급하였습니다.");
		
		// 시간에 따라 변경되는 값을 저장하고 사용할 수 있다.
		// 변수는 하나의 값을 저장하고 사용하기 위한 공간이기보다, 변하는 값을 저장하기 위한 공간이다.
		int sum = 0;
		
		sum = sum + 10;
		
		System.out.println("sum에 10을 더하면 현재 sum의 값은 ? " + sum);
		
		sum = sum + 10;
	}
}
