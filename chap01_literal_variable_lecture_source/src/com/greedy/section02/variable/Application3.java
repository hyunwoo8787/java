package com.greedy.section02.variable;

public class Application3 {

	public static void main(String[] args) {
		int age = 20;
		// int age = 20; // 동일한 변수명을 가지므로 에러 발생
		
		int Age = 30; // 변수명의 대소문자를 구분하므로 age와 Age는 다르기 때문에 사용 가능
		
		// int int = 20; // 변수명이 예약어와 같으므로 에러 발생
		int Int = 20; // 위와 마찬가지로 대소문자를 구분하여 예약어가 아니므로 사용 가능
		
		// int ^%inum = 50; // 변수명으로 '_'나 '$'을 제외한 특수문자는 사용 불가능하다
		short $_hort = 100;
		
		// int 1num = 50; // 변수명을 숫자로 시작해서 에러 발생
		int num1 = 50; // 숫자가 처음에 시작하지 않으면 섞어서 사용 가능
	}
}
