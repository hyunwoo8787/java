package com.greedy.section04.overflow;

public class Application1 {
	public static void main(String[] args) {
		
		/* 오버플로우
		 * 자료형 별 값의 최대 범위를 벗어나는 경우
		 * */
		
		byte num1 = 127;
		
		System.out.println("num1 : " + num1); // 127 : byte의 최대 저장 범위
		
		num1++;
		
		System.out.println("num1 overflow: " + num1); // -128 : byte의 최대값을 벗어나 LSB의 부호비트가 플래그 되어
													  // byte의 최소값으로 출력됨. 비트로 나타내면 1000 0000이 된다
		
		/* 언더플로우
		 * 오버플로우와 반대 개념으로 최소 범위보다 작은 수를 발생시키는 경우 발생하는 현상이다.
		 * */
		
		num1 = -128;
		
		System.out.println("num1 : " + num1); // -128
		
		num1--;
		
		System.out.println("num1 underflow : " + num1);
		// 비트의 변화 1000 0000 = -128
		// -1 이후    0111 1111 = 127
		
		int firstNum = 1_000_000;
		int secondNum = 700_000;
		
		int multi = firstNum * secondNum;
		
		System.out.println("firstNum * secondNum = " + multi);
		
		/* 해결 방법 
		 * 1. 오버 플로우를 예측하고 더 큰 자료형으로 결과값을 받아서 처리한다.
		 * 2. 숫자 타입의 값을 문자열 형태로 변환하여 저장한다.
		 * */
		
		long longMulti = firstNum * secondNum; // 더 큰 자료형인 long을 사용했지만 숫자 계산시 기본적으로 사용되는 
											   // 자료형이 int 이므로 오버플로우 발생
		
		System.out.println("longMulti : " + longMulti);
		
		/* 계산이 처리 되기 전에 long 타입으로 자료형을 변경해줘야한다. (강제형변환) */
		long result = (long)firstNum * secondNum;
		
		System.out.println("result : " + result);
	}
}
