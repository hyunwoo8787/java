package com.greedy.section03.branching_statement;


public class A_break {

    /* break문
     * 반복문 내에서 사용한다.
     * 해당 반복문을 빠져 나올 때 사용하며, 반복문의 조건문 판단 결과와 상관없이 반복문을 빠져나올 수 있다
     * break를 만나면 가장 가까운 반복문을 종료한다.
     * 
     * 일반적으로 if (조건식) { break; } 처럼 사용된다.
     * 단, switch문은 반복문이 아니지만 예외적으로 사용된다.
     * */
    
    public void testSimpleBreakStatement() {
	
	int sum = 0;
	int i = 1;
	while (true) {
	    sum += i;
	    System.out.println(i);
	    
	    if (i == 100) {
		break;
	    }
	    ++i;
	}	
	System.out.println("1부터 100까지의 합은 " + sum + "입니다.");
    }

    public void testSimpleBreakStatement2() {
	
	/* 중첩 반복문 내에서 분기문의 흐름 */
	/* 구구단 2~9단까지 출력 
	 * 단, 각 단의 수가 5보다 큰 경우는 출력을 생략한다.
	 * */
	
	for(int dan = 2; dan < 10; ++dan) {
	    for (int su = 1; su < 10; ++su) {
		if (su > 5) {
		    break; // 가장 가까운 반복문을 나간다. 현재 반복문을 나간다.
		}
		System.out.print(dan + " * " + su + " = " + (dan * su) + "\t");
	    }
	    System.out.println();
	}
	
    }

    /**
     * <pre>
     * break문 실행 흐름을 확인
     * 정첩 반복문 내에서 분기문(break)를 이용하여 한 번에 여러 개의 반복문 중지시키기
     * </pre>
     */
    public void testJumpBreak() {
	
	label:
	for(;;) {
	    for (int i = 0; i < 10; i++) {
		
		System.out.println(i);
		if (i == 3) {
		    break label;
		}
	    }
	}
    }
}
