package com.greedy.section01.conditional_statement;

public class Application1 {
    public static void main(String[] args) {

	A_if aif = new A_if();
	//aif.testSimpleIfStatement();
	//aif.testNestedIfStatement();

	B_IfElse bie = new B_IfElse();
	//bie.testSimpleIfElseStatemnet();
	//bie.testNestedIfElseStatement();

	C_IfElseIf cief = new C_IfElseIf();
	//cief.testSimpleIfElseIfStatement();
	//cief.testNestedIfElseIfStatement();

	D_switch dwi = new D_switch();
	//dwi.testSimpleSwitchStatement();
	dwi.testSwitchVendingMachine();
    }
}
