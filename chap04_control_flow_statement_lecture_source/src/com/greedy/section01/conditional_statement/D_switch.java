package com.greedy.section01.conditional_statement;

import java.util.Scanner;

public class D_switch {

    /**
     * <pre>
     * switch문 실행 흐름을 확인하기 위한 용도의 기능을 제공
     * </pre>
     */
    public void testSimpleSwitchStatement() {

	/* [switch문 표현식]
	 * switch (비교할 변수) {
	 * 		case 비교값1 : 바교값1과 일치하는 경우 실행할 구문; break;
	 * 		case 비교값2 : 비교값2와 일치하는 경우 실행할 구문; break;
	 * 		default : case에 모두 해당하지 않는 경우 실행할 구문; break;
	 * }
	 * 
	 * 여러 개의 비교값 중 일치하는 조건에 해당하는 로직. 실행하는 것은 if-else-if와 유사하다.
	 * 따라서 일부 호환이 가능하다.
	 * 하지만 switch문으로 비교 가능한 값은 정수, 문자, 문자열 형태의 값이다.
	 * 실수와 논리는 비교할 수 없다.
	 * 또한 정확하게 일치하는 경우만 비교할 수 있으며 알파벳의 대소문자비교를 할 수 없다
	 * case절에는 변수를 사용하지 못한다. (값만 가능)
	 * 또한 문자열 비교는 jdk 1.7 (자바7) 이상부터 가능하다.
	 * */

	Scanner sc = new Scanner(System.in);
	System.out.print("첫 번쨰 정수 입력 : ");
	int first = sc.nextInt();
	System.out.print("두 번째 정수 입력 : ");
	int second = sc.nextInt();
	System.out.print("연산 기호 입력 (+, -, *, /, %) : ");
	char op = sc.next().charAt(0);

	int result = 0;

	switch (op) {
	    case '+':
		result = first + second;
		break;
	    case '-':
		result = first - second;
		break;
	    case '*':
		result = first * second;
		break;
	    case '/':
		result = first / second;
		break;
	    case '%':
		result = first % second;
		break;
	    default:
		System.out.println("잘못된 기호를 입력하셨습니다.");
		return;
	}
	System.out.println(first + " " + op + " " + second + " = " + result);
	sc.close();
    }
    
    /**
     * <pre>
     * switch문으로 문자열 값 비교 테스트 및 break 테스트
     * </pre>
     */
    public void testSwitchVendingMachine() {
	
	/* switch문을 이용해서 문자열 값 비교 및 break에 대한 테스트
	 * 1. JDK 1.7이상부터 switch문을 이용해서 문자열을 비교할 수 있다.
	 * 2. break를 사용하지 않으면 멈추지 않고 계속 실행구문을 동작시킨다.
	 * */
	
	System.out.println("============= greedy vending machine ============");
	System.out.println("     사이다      콜라       환타       바카스       핫식스");
	System.out.println("=================================================");
	System.out.print("음료를 선택해주세요 : ");
	
	/* 원하는 음료를 문자열로 입력받는다. */
	Scanner sc = new Scanner(System.in);
	String selectedDrink = sc.nextLine();
	int price = 0;
	
	switch (selectedDrink) {
	    case "사이다":
		System.out.println("사이다를 선택하셨습니다.");
		price = 500;
	    case "콜라":
		System.out.println("콜라를 선택하셨습니다.");
		price = 600;
	    case "환타":
		System.out.println("환타를 선택하셨습니다.");
		price = 550;
	    case "바카스":
		System.out.println("바카스를 선택하셨습니다.");
		price = 2000;
	    case "핫식스":
		System.out.println("핫식스를 선택하셨습니다,");
		price = 1000;
		break;

	    default:
		System.out.println("목록에 없는 음료입니다.");
		break;
	}
	
	System.out.println(price + "원을 투입해주세요.");
	
	
	/* 수정 */
	
	System.out.println("============== 개선된 자판기 ===============");
	String order = "";

	switch (selectedDrink) {
	    case "사이다":
		order = "사이다";
		price = 500;
		break;
	    case "콜라":
		order = "콜라";
		price = 600;
		break;
	    case "환타":
		order = "환타";
		price = 550;
		break;
	    case "바카스":
		order = "바카스";
		price = 2000;
		break;
	    case "핫식스":
		order = "핫식스";
		price = 1000;
		break;
	    default:
		System.out.println("목록에 없는 음료입니다.");
		return;
	}
	System.out.println(order + "를 선택하셨습니다.");
	System.out.println(price + "원을 투입해주세요.");
	sc.close();
    }
}
