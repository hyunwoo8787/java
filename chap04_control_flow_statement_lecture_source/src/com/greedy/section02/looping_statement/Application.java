package com.greedy.section02.looping_statement;

public class Application {
    public static void main(String[] args) {
	
	A_for af = new A_for();
	//af.testSimpleForStatement();
	//af.testForExample1();
	//af.testForExample2();
	//af.testForExample3();
	//af.testForExample4();
	//af.printSimpleGugudan();
	A_nestedFor anst = new A_nestedFor();
	//anst.printUpgradeGugudanFromTwoToNine();
	//anst.printTriangleStars();
	
	B_while bWhile = new B_while();
	//bWhile.testSimpleWhileStatement();
	//bWhile.testWhileExample3();
	
	C_doWhile doWhile = new C_doWhile();
	//doWhile.testSimpleDoWhileStatement();
	doWhile.testDoWhileExample1();
    }
}
