package com.greedy.section02.looping_statement;

import java.util.Scanner;

public class B_while {

    /* [while문 표현식]
     * 초기식;
     * while (조건식) {
     * 	조건을 만족하는 경우 수행할 구문(반복할 구문);
     * 	증감식;
     * }
     * */
    
    public void testSimpleWhileStatement() {
	
	/* 1부터 10까지 1씩 증가시키면서 값을 출력하는 기본 반복문 */
	
	int num = 1;
	while (num <= 10) {
	    System.out.println(num);
	    ++num;
	}
    }
    
    public void testWhileExample1() {
	/* 입력한 문자열의 인덱스를 이용하여 문자를 하나씩 출력해보기 */
	/* charAt(int index) : 문자열에서 인덱스에 해당하는 문자를 char형으로 반환하는 기능 */
	/* length() : String 클래스의 메소드로 문자열의 길이를 int형으로 반환한다. */
	/* index는 0부터 시작하고 마지막 인덱스는 항상 길이보다 한 개 작은 숫자를 가진다.
	 * 만약 존재하지 않는 인덱스에 접근하게 되면 StringIndexOutOfBoundsExcetion이 발생
	 */
	
	Scanner sc = new Scanner(System.in);
	System.out.print("문자열 입력 : ");
	String str = sc.nextLine();
	
	int index = 0;
	while (index < str.length()) {
	    System.out.println(index + " : " + str.charAt(index));
	    ++index;
	}
	sc.close();
    }
    
    public void testWhileExample2() {
	
	/* 정수 하나를 입력 받아서 1부터 입력받은 정수까지의 합계를 구한다. */
	
	Scanner sc = new Scanner(System.in);
	System.out.print("숫자 하나를 입력해주세요 : ");
	int num = sc.nextInt();
	int sum = 0;
	
	System.out.print("1부터 입력 받은 정수 " + num + "까지의 합은 ");
	while (num > 0) {
	    sum += num;
	    --num;
	}
	System.out.println(sum + "입니다");
	sc.close();
    }
    
    public void testWhileExample3() {
	
	/* 구구단 2단부터 9단까지 모두 출력해보자 */
	
	int dan = 2;
	while (dan < 10) {
	    int su = 1;
	    while (su < 10) {
		System.out.print(dan + " * " + su + " = " + (dan * su) + "\t");
		++su;
	    }
	    System.out.println();
	    ++dan;
	}
    }
}
