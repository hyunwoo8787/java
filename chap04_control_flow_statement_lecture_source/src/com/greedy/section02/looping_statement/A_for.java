package com.greedy.section02.looping_statement;

import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.HTMLReader.SpecialAction;

public class A_for {

    /**
     * <pre> 
     * 단순 for문 실행 흐름을 확인하기 위한 용도의 기능을 제공
     * </pre>
     */
    public void testSimpleForStatement() {
	
	/* [for문 표현식]
	 * for (초기식; 조건식; 증감식) {
	 * 	조건을 만족하는 경우 수행할 구문(반복할 구문);
	 * }
	 * */
	/* 1부터 10까지 1씩 증가시키면서 값을 출력하는 기본 반복문 */
	
	for (int i = 0; i < 10; i++) {
	    System.out.println(i+1);
	}
    }
    
    /**
     * <pre>
     * 무엇을 반복하면 좋을지 생각해보는 예제1
     * </pre>
     */
    public void testForExample1() {
	
	Scanner sc = new Scanner(System.in);
	
	/*
	 * System.out.print("1 번째 학생의 이름을 입력해주세요 : "); String student1 = sc.nextLine();
	 * System.out.println("1 번째 학생의 이름은 " + student1 + "입니다.");
	 * 
	 * System.out.print("2 번째 학생의 이름을 입력해주세요 : "); String student2 = sc.nextLine();
	 * System.out.println("2 번째 학생의 이름은 " + student2 + "입니다.");
	 * 
	 * System.out.print("3 번째 학생의 이름을 입력해주세요 : "); String student3 = sc.nextLine();
	 * System.out.println("3 번째 학생의 이름은 " + student3 + "입니다.");
	 * 
	 * System.out.print("4 번째 학생의 이름을 입력해주세요 : "); String student4 = sc.nextLine();
	 * System.out.println("4 번째 학생의 이름은 " + student4 + "입니다.");
	 * 
	 * System.out.print("5 번째 학생의 이름을 입력해주세요 : "); String student5 = sc.nextLine();
	 * System.out.println("5 번째 학생의 이름은 " + student5 + "입니다.");
	 * 
	 * System.out.print("6 번째 학생의 이름을 입력해주세요 : "); String student6 = sc.nextLine();
	 * System.out.println("6 번째 학생의 이름은 " + student6 + "입니다.");
	 * 
	 * System.out.print("7 번째 학생의 이름을 입력해주세요 : "); String student7 = sc.nextLine();
	 * System.out.println("7 번째 학생의 이름은 " + student7 + "입니다.");
	 * 
	 * System.out.print("8 번째 학생의 이름을 입력해주세요 : "); String student8 = sc.nextLine();
	 * System.out.println("8 번째 학생의 이름은 " + student8 + "입니다.");
	 * 
	 * System.out.print("9 번째 학생의 이름을 입력해주세요 : "); String student9 = sc.nextLine();
	 * System.out.println("9 번째 학생의 이름은 " + student9 + "입니다.");
	 * 
	 * System.out.print("10 번째 학생의 이름을 입력해주세요 : "); String student10 =
	 * sc.nextLine(); System.out.println("10 번째 학생의 이름은 " + student10 + "입니다.");
	 * 
	 * System.out.println("10명의 학생 이름을 입력 받고 출력하는 기능을 완료했습니다."); sc.close();
	 */
	
	/* 반복해야 하는 내용은
	 * 1. 안내문구 출력
	 * 2. 학생 이름 입력받아 변수에 저장
	 * 3. 저장된 이름을 출력
	 * 
	 * 반복횟수는 1부터 10까지 1씩 증가하여 총 10번을 반복
	 * */
	
	int i = 0;
	for (; i < 2; i++) {
	    System.out.print((i + 1) + " 번째 학생의 이름을 입력해주세요 : ");
	    String student = sc.nextLine();
	    System.out.println((i + 1) + " 번째 학생의 이름은 " + student + "입니다.");
	}

	System.out.println(i + "명의 학생 이름을 입력 받고 출력하는 기능을 완료했습니다.");
	sc.close();
    }
    
    /**
     * <pre>
     * 무엇을 반복하면 좋을지 생각해보는 예제2
     * </pre>
     */
    public void testForExample2() {
	
	/* 1 ~ 10까지의 합계를 구하시오 */
	
	int num1 = 1;
	int num2 = 2;
	int num3 = 3;
	int num4 = 4;
	int num5 = 5;
	int num6 = 6;
	int num7 = 7;
	int num8 = 8;
	int num9 = 9;
	int num10 = 10;
	
	/* 결과를 누적시켜 담아줄 변수 선언 */
	int sum = 0;
	
	sum += num1;
	sum += num2;
	sum += num3;
	sum += num4;
	sum += num5;
	sum += num6;
	sum += num7;
	sum += num8;
	sum += num9;
	sum += num10;
	
	System.out.println("sum : " + sum);
	
	/* 반복해야 할 내용 
	 * 1. 변수에 1씩 증가하는 값 담기
	 * 2. 저장된 값을 sum에 누적시키기
	 * 
	 * 반복횟수는? 1부터 10까지 1씩 증가 (10회 반복)
	 * 
	 * 반복하지 않을 내용
	 * 1. 값을 누적해서 저장할 sum을 초기화
	 * 2. sum에 누적된 값 출력
	 * */
	int sum2 = 0;
	for (int i = 0; i < 10; i++) {
	    sum2 += i + 1;   
	}
	
	System.out.println("sum2 : " + sum);
	
    }
    
    /**
     * <pre>
     * 무엇을 반복하면 좋을지 생각해보는 예제3
     * </pre>
     */
    public void testForExample3() {
	
	/* 5 ~ 10 사이의 난수를 발생시켜서
	 * 1부터 발생한 나수까지의 합계를 구해보자.
	 * 
	 * 5 : 1+2+3+4+5
	 * 6 : 1+2+3+4+5+6
	 * */
	
	int end = (int)(Math.random() * 6) + 5;
	int result = (1 + end) * end / 2;
	
	System.out.println("1부터 " + end + "까지의 합은 : " + result);
    }
    
    /**
     * <pre>                             
     * 무엇을 반복하면 좋을지 생각해보는 예제3            
     * </pre>                            
     */
    public void testForExample4() {
	
	/* 숫자 두 개를 입력 받아 작은 수에서 큰 수까지의 합계를 구하세요.
	 * 단, 두 숫자는 같은 숫자를 입력하지 않는다는 가정으로
	 * */
	
	Scanner sc = new Scanner(System.in);
	
	System.out.print("첫번째 숫자를 입력하세요 : ");
	int num1 = sc.nextInt();
	System.out.print("두번째 숫자를 입력하세요 : ");
	int num2 = sc.nextInt();
	
	int max = Math.max(num1, num2);
	int min = Math.min(num1, num2);
	int result = 0;
	
	for (int i = min; i <= max; i++) {
	    result += i;
	}
	
	System.out.println(min + "에서 " + max + "까지의 합은 " + result + "입니다.");
    }
    
    public void printSimpleGugudan() {
	
	/* 키보드로 2~9 사이의 구구단을 입력 받아
	 * 2~9 사이인 경우 해당 단의 구구단을 출력하고,
	 * 그렇지 않은 경우 "반드시 2~9 사이의 양수를 입력해야한다."를 출력
	 * */
	
	Scanner sc = new Scanner(System.in);
	System.out.print("2~9 사이의 숫자를 입력해주세요. : ");
	int num = sc.nextInt();
	if (num >= 2 && num < 10) {
	    
	    for (int i = 1; i < 10; i++) {
		System.out.println(num + " * " + i + " = " + (num * i));
	    }
	} else {
	    System.out.println("반드시 2~9 사이의 양수를 입력해야합니다.");
	}
	sc.close();
    }
}
