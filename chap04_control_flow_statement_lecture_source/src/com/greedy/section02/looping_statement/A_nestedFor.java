package com.greedy.section02.looping_statement;

import java.util.Scanner;

public class A_nestedFor {

    public void printGugudanFromTwoToNine() {

	for (int i = 1; i < 10; i++) {
	    for (int j = 2; j < 10; j++) {
		System.out.print(j + " * " + i + " = " + (i * j) + "	");
	    }
	    System.out.println();
	}
    }

    public void printUpgradeGugudanFromTwoToNine() {

	for (int i = 1; i < 10; i++) {
	    printGugudanOf(i);
	    System.out.println();
	}
    }

    public void printGugudanOf(int i) {
	for (int j = 2; j < 10; j++) {
	    System.out.print(j + " * " + i + " = " + (i * j) + "\t");
	}
    }
    
    public void printStarInputRowTimes() {
	
	Scanner sc = new Scanner(System.in);
	System.out.print("출력할 행 수를 입력하세요 : ");
	int row = sc.nextInt();
	
	for (int i = 1; i <= row; i++) {
	    for (int j = 1; j < 6; j++) {
		System.out.print("*");
	    }
	    System.out.println();
	}
	sc.close();
    }
    
    public void printTriangleStars() {
	Scanner sc = new Scanner(System.in);
	System.out.print("출력할 행 수를 입력하세요 : ");
	int row = sc.nextInt();
	
	for (int i = 1; i <= row; i++) {
	    for (int j = 1; j <= i; j++) {
		System.out.print("*");
	    }
	    System.out.println();
	}
	sc.close();
    }
}
