package com.greedy.section05.calendar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Application2 {

    public static void main(String[] args) {

        /* 1. getInstance() static 메소드 이용 */
        Calendar calendar = Calendar.getInstance();

        System.out.println(calendar);

        /* 2. GregorianCalendar 이용하는 방법 */
        /* 2-1. 기본생성자 사용 */
        Calendar gregorianCalendar = new GregorianCalendar();

        System.out.println(gregorianCalendar);

        /* 2-2. 년, 월, 일, 시, 분, 초 정보를 이용해서 인스턴스를 생성 */
        int year = 2023;
        int month = 3; // 월은 0~11월로 사용
        int dayOfMonth = 14;
        int hour = 13;
        int min = 59;
        int second = 15;

        Calendar day = new GregorianCalendar(year, month, dayOfMonth, hour, min, second);
        System.out.println(day);

        Date date = new Date(day.getTimeInMillis());
        System.out.println(date);

        int dayYear = day.get(1);
        int dayMonth = day.get(2);
        int dayDayOfMonth = day.get(5);
        System.out.println(dayYear);
        System.out.println(dayMonth);
        System.out.println(dayDayOfMonth);

        System.out.println(Calendar.YEAR);
        System.out.println(Calendar.MONTH);
        System.out.println(Calendar.DATE);

        System.out.println("Year : " + day.get(Calendar.YEAR));
        System.out.println("Month : " + day.get(Calendar.MONTH));
        System.out.println("DayOfMonth : " + day.get(Calendar.DAY_OF_MONTH));
        /* 요일[ 일(1), 월(2), 화(3), 수(4), 목(5), 금(6), 토(7) ]의 의미 */
        System.out.println("DayOfWeek : " + day.get(Calendar.DAY_OF_WEEK));

        System.out.println("hourOfDay : " + day.get(Calendar.HOUR_OF_DAY)); // 24시간 체계
        System.out.println("hour : " + day.get(Calendar.HOUR));             // 12시간 체계

        System.out.println("AM/FM : " + day.get(Calendar.AM_PM));
    }
}
