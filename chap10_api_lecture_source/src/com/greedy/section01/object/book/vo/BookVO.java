package com.greedy.section01.object.book.vo;

import java.util.Objects;

public class BookVO {

    private int number;
    private String title;
    private String author;
    private int price;

    public BookVO() {}

    public BookVO(int number, String title, String author, int price) {
        super();
        this.number = number;
        this.title = title;
        this.author = author;
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BookVO [number=" + number + ", title=" + title + ", author=" + author + ", price=" + price + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, number, price, title);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BookVO other = (BookVO) obj;
        return Objects.equals(author, other.author) && number == other.number && price == other.price
                && Objects.equals(title, other.title);
    }

//    @Override
//    public boolean equals(Object obj) {
//        /* 두 인스턴스의 주소가 같으면 이후 다른 내용을 비교하지 않고 true 반환 */
//        if (this == obj) {
//            return true;
//        }
//
//        /* this는 인스턴스가 생성되면 주소값이 저장된다. null일 수 없다. */
//        if (obj == null) {
//            return false;
//        }
//
//        /* 서로 같은 클래스인지 비교 */
//        if (this.getClass() != obj.getClass()) {
//            return false;
//        }
//
//        /* 전달받은 레퍼런스 변수 안에 있는 각 필드별로 비교를 시작 */
//        BookVO other = (BookVO) obj;
//
//        /* number 필드의 값이 서로 같지 않은 경우 서로 다른 인스턴스이다. */
//        if (number != other.number) {
//            return false;
//        }
//
//        if (title == null) {
//            if (other.title != null) {
//                return false;
//            }
//        } else if (!title.equals(other.title)) {
//            return false;
//        }
//
//        if (author == null) {
//            if (other.author != null) {
//                return false;
//            }
//        } else if (!author.equals(other.author)) {
//            return false;
//        }
//
//        if (price != other.price) {
//            return false;
//        }
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//
//        /* 곱셈 연산을 누적시켜야 하기 때문에 0이 아닌 값으로 초기화 */
//        final int PRIME = 31;
//        int result = 1;
//
//        result = PRIME * result + number;
//        result = PRIME * result + author.hashCode();
//        result = PRIME * result + title.hashCode();
//        result = PRIME * result + price;
//
//        return result;
//    }
}
