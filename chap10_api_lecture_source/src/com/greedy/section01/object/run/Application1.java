package com.greedy.section01.object.run;

import com.greedy.section01.object.book.vo.BookVO;

public class Application1 {

    public static void main(String[] args) {

        /* 모든 클래스는 Object 클래스의 후손이다.
         *
         * 따라서 Object 클래스가 가진 메소드를 자신의 것처럼 사용할 수 있다.
         * 또한 부모 클래스가 가지는 메소드를 오버라이딩 해서 사용하는 것도 가능하다.
         * */

        BookVO book1 = new BookVO(1, "홍길동전", "허균", 50_000);
        BookVO book2 = new BookVO(2, "목민심서", "정약용", 30_000);
        BookVO book3 = new BookVO(2, "목민심서", "정약용", 30_000);

        /* java.lang.Object 클래스의 toString() 메소드를 사용
         * 인스턴스가 생성될 때 사용한 full class name과 @ 그리고 16진수 해시코드가 문자열로 반환된다. */
        System.out.println("book1.toString() : " + book1.toString());
        System.out.println("book2.toString() : " + book2.toString());
        System.out.println("book3.toString() : " + book3.toString());

        System.out.println();
        System.out.println("book1 : " + book1);
        System.out.println("book2 : " + book2);
        System.out.println("book3 : " + book3);
    }
}
