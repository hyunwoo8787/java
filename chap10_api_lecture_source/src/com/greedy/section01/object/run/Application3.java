package com.greedy.section01.object.run;

import java.util.HashMap;
import java.util.Map;

import com.greedy.section01.object.book.vo.BookVO;

public class Application3 {

    public static void main(String[] args) {

        /* Object클래스의 명세에 작성된 일반 규약에 따르면
         * equals() 메소드를 재정의 하는 경우 반드시 hashCode() 메소드도 재정의하도록 되어 있다.
         *
         * 만약 hashCode()를 재정의 하지 않으면 같은 값을 가지는 동등객체는
         * 같은 해시코드값을 가져야 한다는 규약에 위반되게 된다. (강제성은 없지만 규약대로 작성하는 것이 좋다.) */

        BookVO book1 = new BookVO(1, "홍길동전", "허균", 50_000);
        BookVO book2 = new BookVO(1, "홍길동전", "허균", 50_000);

        System.out.println("book1.hashCode : " + book1.hashCode());
        System.out.println("book2.hashCode : " + book2.hashCode());

        /* Map은 key와 value를 쌍으로 저장하는 자료구조로 상속받아서 구현한 HashMap에
         * key로 홍길동전 정보를 가지는 BookVO타입의 인스턴스를 사용
         *
         * HashMap에서 홍길동전 정보를 가지는 인스턴스로 값을 꺼내오려 할 때
         * hashCode가 재정의되어 동일한 필드인 경우 hashCode가 반환되도록 작성되어 있지 않다면
         * 결과값이 예상과 다르게 동작 */
        Map<BookVO, String> map = new HashMap<>();
        map.put(new BookVO(1, "홍길동전", "허균", 50_000), "selled");

        String str = map.get(new BookVO(1, "홍길동전", "허균", 50_000));
        int a = 0;
        System.out.println(str);
    }
}
