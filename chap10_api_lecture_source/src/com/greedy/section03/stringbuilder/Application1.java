package com.greedy.section03.stringbuilder;

public class Application1 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("java");

        /* toString이 오버라이딩 되어있다. */
        System.out.println("sb : " + sb.toString());

        System.out.println("sb의 hashCode : " + sb.hashCode());

        /* 문자열 수정 */
        sb.append("oracle");
        System.out.println("sb : " + sb);
        System.out.println("sb의 hashCode : " + sb.hashCode());
    }
}
