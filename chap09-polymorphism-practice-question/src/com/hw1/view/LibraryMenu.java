package com.hw1.view;

import java.util.Scanner;

import com.hw1.controller.LibraryManager;
import com.hw1.model.dto.Book;
import com.hw1.model.dto.Member;

public class LibraryMenu {

    private LibraryManager lm = new LibraryManager();
    Scanner sc = new Scanner(System.in);

    public void mainMenu() {
        // 이름, 나이, 성별을 키보드로 입력 받은 후 Member 객체 생성
        // LibraryManager의 insertMember() 메소드에 전달

        System.out.print("이름을 입력해주세요 : ");
        String name = sc.next();
        System.out.print("나이를 입력해주세요 : ");
        int age = sc.nextInt();
        System.out.print("성별을 입력해주세요 : ");
        char gender = sc.next().charAt(0);

        lm.insertMember(new Member(name, age, gender));

        end:
        while (true) {
            System.out.println("==== 메뉴 ====");
            System.out.println("1. 마이페이지"); // LibraryManager의 myInfo() 호출하여 출력
            System.out.println("2. 도서 전체 조회");// LibraryMenu의 selectAll() 호출
            System.out.println("3. 도서 검색"); // LibraryMenu의 searchBook() 호출
            System.out.println("4. 도서 대여하기"); // LibraryMenu의 rentBook() 호출
            System.out.println("0. 프로그램 종료하기");
            System.out.print("번호를 입력해주세요 ㅣ ");
            int key = sc.nextInt();

            switch (key) {
                case 1: System.out.println(lm.myInfo().toString()); break;
                case 2: selectAll(); break;
                case 3: searchBook(); break;
                case 4: rentBook(); break;
                case 0: System.out.println("프로그램을 종료합니다."); break end;
                default:
            }
        }
    }

    public void selectAll() {
        Book[] books = lm.selectAll();

        for (int i = 0; i < books.length; i++) {
            System.out.println((i + 1) + "번도서 : " + books[i].toString());
        }
    }

    public void searchBook() {
        sc.nextLine();
        System.out.print("검색할 책의 제목을 입력해주세요. : ");
        String name = sc.nextLine();

        Book[] result = lm.searchBook(name);

        for (Book book : result) {
            System.out.println(book.toString());
        }
    }

    public void rentBook() {
        sc.nextLine();
        System.out.print("대여하실 도서의 번호를 입력해주세요 : ");
        int idx = sc.nextInt();

        int result = lm.rentBook(idx);

        if (result == 0) {
            System.out.println("성공적으로 대여되었습니다.");
        } else if (result == 1) {
            System.out.println("나이 제한으로 대여 불가능입니다.");
        } else if (result == 2){
            System.out.println("성공적으로 대여되었습니다. 요리학원 쿠폰이 발급되었습니다. 마이페이지를 통해 확인하세요.");
        } else {
            System.out.println("잘못된 번호를 입력하셨습니다.");
        }
    }
}
