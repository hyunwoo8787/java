package com.greedy.section01.extend;

public class Application {

    public static void main(String[] args) {
        FireCar fireCar = new FireCar();

        fireCar.soundHorn();
        fireCar.run();
        fireCar.soundHorn();
        fireCar.stop();
        fireCar.soundHorn();

        /* 부모의 멤버 중에 접근제한자가 private이면 접근이 불가능하다. */
//        fireCar.runningStatus; // private 속성 접근 불가
//        fireCar.isRunning();   // private 메소드 접근 불가하지만 protected로 변경하면 사용 가능
        fireCar.sprayWater();

        RacingCar racingCar = new RacingCar();

        racingCar.soundHorn();
        racingCar.run();
        racingCar.soundHorn();
        racingCar.stop();
        racingCar.soundHorn();
    }
}
