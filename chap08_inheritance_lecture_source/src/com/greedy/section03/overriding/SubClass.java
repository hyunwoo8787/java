package com.greedy.section03.overriding;

public class SubClass extends SuperClass {

    /* 1. 메소드 이름 변경하면 에러 발생
     * 2. 메소드의 리턴타입 변경하면 에러 발생
     * 3. 매개변수 개수나 타입이 변경되면 에러 발생
     * */
    @Override
    public void method(int num) {}

    /* @Override 어노테이션을 붙이지 않고
     * 매개변수 개수 또는 타입을 변경하면 오버로딩이 성립되어 사용 가능
     * */
    public int method(int num, int num2) { return num + num2; }

    /* 4. private 메소드는 오버라이딩 불가*/
//    @Override
//    private void privateMethod() {}

    /* 5. final 메소드 오버라이딩 불가 */
//    @Override
//    public final void finalMethod() {}

    /* 6. 부모 메소드의 접근제한자와 같거나 더 넓은 범위로 오버라딩 가능 */

//    @Override
//    private void protectedMethod() {} // 더 좋은 범위로 불가능

//    @Override
//    void protectedMethod() {} // 더 좁은 범위로 불가능

//    @Override
//    protected void protectedMethod() {} // 같은 범위로 가능

    @Override
    public void protectedMethod() {} // 더 넓은 범위로도 가능
}
