package com.greedy.section02.superkeyword;

import java.util.Date;

public class ComputerDTO extends ProductDTO {
    /* ComputerDTO 클래스는 하나의 상품이다. (IS-A) */
    private String operationSystem;
    private String cpu;
    private int hdd;
    private int ram;

    public ComputerDTO() {
        System.out.println("ComputerDTO 클래스의 기본 생성자 호출...");
    }

    public ComputerDTO(String code, String brand, String name, int price, Date manufacturingDate,
                String operationSystem, String cpu, int hdd, int ram) {
        /* 부모의 모든 필드를 초기화하는 생성자로 ProductDTO 클래스가 가진 필드를 초기화 할 값 전달 */
        super(code, brand, name, price, manufacturingDate);

        this.operationSystem = operationSystem;
        this.cpu = cpu;
        this.hdd = hdd;
        this.ram = ram;

        System.out.println("ComputerDTO 클래스의 부모 필드도 초기화하는 생성자 호출...");

    }

    /* 부모 기본생성자를 호출하고 나머지 자식클래스의 필드를 초기화함 */
    public ComputerDTO(String operationSystem, String cpu, int hdd, int ram) {
        super();
        this.operationSystem = operationSystem;
        this.cpu = cpu;
        this.hdd = hdd;
        this.ram = ram;

        System.out.println("ComputerDTO 클래스의 매개변수 생성자 호출...");
    }

    public String getOperationSystem() {
        return operationSystem;
    }

    public void setOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public int getHdd() {
        return hdd;
    }

    public void setHdd(int hdd) {
        this.hdd = hdd;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    @Override
    public String getInformation() {

//        return "code = " + super.getCode()
//            + ", brand = " + super.getBrand()
//            + ", name = " + super.getName()
//            + ", price = " + super.getPrice()
//            + ", manufacturingDate = " + super.getManufacturingDate()
//            + ", cpu = " + cpu
//            + ", hdd = " + hdd
//            + ", ram = " + ram
//            + ", operationSystem = " + operationSystem;

        return super.getInformation()
                + ", cpu = " + cpu
                + ", hdd = " + hdd
                + ", ram = " + ram
                + ", operationSystem = " + operationSystem;
    }


}
