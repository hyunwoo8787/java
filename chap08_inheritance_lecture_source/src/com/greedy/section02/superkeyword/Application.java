package com.greedy.section02.superkeyword;

public class Application {

    public static void main(String[] args) {

        ProductDTO product1 = new ProductDTO();
        System.out.println(product1.getInformation());

        ProductDTO product2 = new ProductDTO("S-123456", "삼성", "갤럭시Z폴드2", 2_398_000, new java.util.Date());
        System.out.println(product2.getInformation());

        ComputerDTO computer1 = new ComputerDTO();
        System.out.println(computer1.getInformation());

        ComputerDTO computer2 = new ComputerDTO("안드로이드", "퀄컴 스냅 드래곤", 521, 12);
        System.out.println(computer2.getInformation());

        ComputerDTO computer3 = new ComputerDTO("S-123456", "삼성", "갤럭시Z폴드2", 2_398_000, new java.util.Date(), "안드로이드", "퀄컴 스냅 드래곤", 521, 12);
        System.out.println(computer3.getInformation());
    }
}
